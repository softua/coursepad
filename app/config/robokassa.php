<?php
/**
 * Created by Ruslan Koloskov
 * Date: 27.09.2014
 * Time: 17:21
 */

return [
	'url' => getenv('ROBOKASSA_URL'),
	'login' =>getenv('ROBOKASSA_LOGIN'),
	'pass1' => getenv('ROBOKASSA_PASS1'),
	'pass2' => getenv('ROBOKASSA_PASS2'),
];