<?php
/**
 * Created by Ruslan Koloskov
 * Date: 22.09.2014
 * Time: 12:08
 */

class Data
{
	private static $data = [];

	private static function has($key)
	{
		if (array_key_exists($key, self::$data)) {
			return true;
		}
		else {
			return false;
		}
	}

	public static function set($key, $value)
	{
		self::$data[$key] = $value;
	}

	public static function get($key)
	{
		if (self::has($key)) {
			return self::$data[$key];
		}
		else {
			return null;
		}
	}

	public static function remove($key)
	{
		if (array_key_exists($key, self::$data)) {
			unset(self::$data[$key]);
		}
	}
} 