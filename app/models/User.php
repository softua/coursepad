<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Faker\Factory as Faker;

/**
 * User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $activation_code
 * @property integer $confirmed
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Role[] $roles
 * @property-read \School $school
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereContactName($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereContactPhone($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereActivationCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereConfirmed($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereRememberToken($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value) 
 */
class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['email', 'password', 'contact_name', 'contact_phone', 'activation_code'];

	public function roles()
	{
		return $this->belongsToMany('Role');
	}

	public function school()
	{
		return $this->hasOne('School');
	}

	public function sendActivationEmail()
	{
		$activationUrl = action(
			'UserController@getActivate',
			[
				'userId' => $this->id,
				'activationCode' => $this->activation_code
			]
		);

		Mail::send('emails.activation', ['activationUrl' => $activationUrl], function($message) {
			$message->from(getenv('MAIL_FROM.address'), getenv('MAIL_FROM.name'));
			$message->to($this->email);
			$message->subject('Coursepad Test!');
		});
	}

	public function register()
	{
		$faker = Faker::create();

		$this->password = Hash::make($this->password);
		$this->activation_code = $faker->sha1;

		if ($this->save()) {
			$this->roles()->attach(3);
			return true;
		}
		else return false;
	}

	public static $rules = [
		'email' => 'required|email|unique:users,email',
		'password' => 'required|between:5,20|confirmed',
		'contact_name' => 'alpha_dash_space',
		'contact_phone' => 'alpha_num',
	];
}
