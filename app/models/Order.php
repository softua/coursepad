<?php

use Carbon\Carbon;
use Laracasts\Commander\Events\EventGenerator;
use Site\Commands\Course\BuyCourseCommand;

class Order extends \Eloquent {

    use EventGenerator;

    protected $fillable = ['group_id', 'amount', 'booking'];

	protected $dates = ['booking', 'created_at', 'updated_at'];

    /**
     * @param BuyCourseCommand $command
     * @param Group            $group
     * @param                  $payer
     * @param                  $listeners
     */
    public static function createNewOrder($group, $payer, $listeners, $countListeners)
    {
        $order           = new static;
        $order->amount   = $group->getFullCourseCost($countListeners);
        $order->group_id = $group->id;
        $order->booking  = Carbon::now(); //date_format(new DateTime('+1 day'), 'Y-m-d H:i');
        $order->save();

        $order->clients()->save($payer);
        $order->clients()->saveMany($listeners);

        return $order;
    }

    public function clients()
    {
        return $this->hasMany('Client');
    }

    public function group()
    {
        return $this->belongsTo('Group');
    }
}