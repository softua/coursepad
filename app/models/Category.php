<?php

class Category extends \Eloquent {

	protected $table = 'categories';
	protected $fillable = [];

	public function parent()
	{
		return $this->belongsTo('Category', 'parent_id');
	}

	public function childrens()
	{
		return $this->hasMany('Category', 'parent_id');
	}

	public function courses()
	{
		return $this->hasMany('Course');
	}

	public function scopeMainCats($query)
	{
		return $query->whereParentId(null);
	}

	public function scopeChildren($query, $id)
	{
		return $query->whereParentId($id)->sorted();
	}

	public function scopeSorted($query)
	{
		return $query->orderBy('sort');
	}
}