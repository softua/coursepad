<?php

/**
 * Role
 *
 * @property integer $id
 * @property string $role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\Role whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Role whereRole($value) 
 * @method static \Illuminate\Database\Query\Builder|\Role whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Role whereUpdatedAt($value) 
 */
class Role extends \Eloquent {
	protected $fillable = [];

	public function users()
	{
		return $this->belongsToMany('User');
	}
}