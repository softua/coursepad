<?php
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Group
 */
class Group extends Eloquent {

    use PresentableTrait;

    /**
     * @var string
     */
    protected $presenter = 'Site\Presenters\GroupPresenter';

    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $dates = ['begins', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('Course');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('Order');
    }

    /**
     * @param $q
     *
     * @return mixed
     */
    public function scopeActive($q)
    {
        return $q->whereStatus(1);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeVisible($query)
    {
        return $query->whereVisibility(1);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeSorted($query)
    {
        return $query->orderBy('begins');
    }

    /**
     * @return int
     */
    public function getSeatsLeft()
    {
        return $this->seats_all - $this->seats_busy;
    }

    public function getFullCourseCost($listenersCount)
    {
        return $this->price*$listenersCount;
    }
}