<?php

/**
 * School
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $short_description
 * @property string $img_url
 * @property integer $status
 * @property boolean $confirmed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\School whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\School wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereContactName($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereContactPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereImgUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereUpdatedAt($value)
 * @property string $title
 * @property string $seo_title
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $courses
 * @method static \Illuminate\Database\Query\Builder|\School whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\School whereSeoTitle($value)
 * @property integer $user_id
 * @property-read \User $user
 * @property-read mixed $first_image
 * @method static \Illuminate\Database\Query\Builder|\School whereUserId($value) 
 * @method static \School active() 
 */
class School extends \Eloquent {
	protected $fillable = ['title', 'short_description'];

	public function courses()
	{
		return $this->hasMany('Course');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function image()
	{
		return $this->hasOne('SchoolImage');
	}

	public function scopeActive($q)
	{
		return $q->whereStatus(1);
	}

	// TODO нужно добавить телефон школе. Приэтом не забыть про создание школы и т.д.
}