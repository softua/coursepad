<?php
use Site\Commands\Course\BuyCourseCommand;

/**
 * Class Client
 */
class Client extends \Eloquent {

    /**
     * @var string
     */
    protected $table = 'clients';
    /**
     * @var array
     */
    protected $fillable = ['last_name', 'first_name', 'email', 'payer', 'listener'];

    /**
     * @param array $listenerData
     *
     * @return static
     */
    public static function createListener($listenerData)
    {
        $listener           = static::create($listenerData);
        $listener->payer    = false;
        $listener->listener = true;
        $listener->save();

        return $listener;
    }

    /**
     * @param BuyCourseCommand $command
     *
     * @return static
     */
    public static function createPayer($command)
    {
        $payer = static::create([
            'last_name'  => $command->last_name,
            'first_name' => $command->first_name,
            'email'      => $command->email,
            'phone'      => $command->phone,
        ]);

        $payer->payer    = true;
        $payer->listener = ($command->me_will_go) ? true : false;
        $payer->save();

        return $payer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Order');
    }
}