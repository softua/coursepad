<?php

/**
 * Level
 *
 * @property integer $id
 * @property string $level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Level whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereLevel($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereUpdatedAt($value) 
 */
class Level extends \Eloquent {
	protected $fillable = [];

	public function courses()
	{
		return $this->hasMany('Course');
	}
}