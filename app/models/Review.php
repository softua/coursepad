<?php

/**
 * Review
 *
 * @property integer $id
 * @property string $username
 * @property integer $rating
 * @property string $description
 * @property integer $course_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Review whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereRating($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Review whereUpdatedAt($value)
 */
class Review extends \Eloquent {
	protected $fillable = [];
}