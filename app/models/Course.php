<?php

use Laracasts\Presenter\PresentableTrait;

class Course extends Eloquent
{

	use PresentableTrait;

	public static $validationRules = [
		'title' => 'required|min:2',
		'lessons' => 'required|numeric',
		'duration' => ['required', 'regex:/^\d+([\.|,]\d+)?$/'],
		'category_id' => 'required',
		'level_id' => 'required',
		'age_id' => 'required',
	];
	protected $table = 'courses';
	protected $fillable = [];


	/**
	 * @var string
	 */
	protected $presenter = 'Site\Presenters\CoursePresenter';

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function school()
	{
		return $this->belongsTo('School');
	}

	public function age()
	{
		return $this->belongsTo('Age', 'age_id');
	}

	public function level()
	{
		return $this->belongsTo('Level');
	}

	public function getFirstImageAttribute($value)
	{
		return array_get($value, 0);
	}

	public function getImagesAttribute($value)
	{
		if ($value) {
			return unserialize($value);
		} else {
			return [];
		}
	}

	public function setImagesAttribute($value)
	{
		$this->attributes['images'] = null;
		if ($value) {
			$this->attributes['images'] = serialize(explode(',', $value));
		}
	}

	public function getMetroAttribute()
	{
		if ($this->location_metro) {
			$matches = [];
			preg_match('/^.*(метро.*)$/', $this->location_metro, $matches);

			return $matches[count($matches) - 1];
		} else {
			return null;
		}
	}

	public function getFirstGroupAttribute()
	{
		return $this->groups->first();
	}

	public function groups()
	{
		return $this->hasMany('Group');
	}

	public function setTitle($value)
	{
		$this->title = trim(strip_tags($value));
		$tempSeoTitle = Translit::get_seo_keyword($this->title, true);
		if ($this->id) {
			$matches = Course::whereSeoTitle($tempSeoTitle)->where('id', '<>', $this->id)->get()->count();
		} else {
			$matches = Course::whereSeoTitle($tempSeoTitle)->get()->count();
		}

		if ($matches) {
			$this->seo_title = $tempSeoTitle . '_' . rand(1, 1000);
		} else {
			$this->seo_title = $tempSeoTitle;
		}
	}

	public function scopeActive($query)
	{
		return $query->whereStatus(1);
	}

	public function scopeSorted($query)
	{
		return $query->orderBy('created_at', 'desc')->orderBy('updated_at', 'desc');
	}

	public function averageSeatsCount()
	{
		$countAll = 0;

		foreach ($this->groups as $group) {
			$countAll += $group->seats_all;
		}

		return round($countAll / count($this->groups));
	}

	public function getTotalDuration()
	{
		return ceil($this->lessons * $this->duration);
	}

	public function metro()
	{
		return $this->belongsTo('MetroStation');
	}

	public function generateSlug()
	{
		$seo_title = Translit::get_seo_keyword($this->title, true);
		while (true)
        {
			$course = self::whereSeoTitle($seo_title)->first();
			if ($course)
            {
				$seo_title = Translit::get_seo_keyword($this->title, true) . '_' . rand(2, 1000);
			}
            else
            {
                return $seo_title;
                break;
            }
		}
	}
}