<?php

/**
 * Age
 *
 * @property integer $id
 * @property string $age
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $courses
 * @method static \Illuminate\Database\Query\Builder|\Age whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Age whereAge($value) 
 * @method static \Illuminate\Database\Query\Builder|\Age whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Age whereUpdatedAt($value) 
 */
class Age extends \Eloquent {
	protected $fillable = [];

	public function courses()
	{
		return $this->hasMany('Course', 'age_id');
	}
}