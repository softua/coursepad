<?php

class MetroStation extends \Eloquent {
	protected $table = 'metro_stations';
	protected $fillable = ['name'];

	public function courses()
	{
		return $this->hasMany('Course');
	}

	/**
	 * @param String $name
	 * @return int $id
	 */
	public static function getMetroId($name)
	{
		$metro = self::whereName($name)->first();
		if (!$metro && $name)
		{
			$metro = MetroStation::create([
				'name' => $name,
			]);

			return $metro->id;
		}
		elseif ($metro)
		{
			return $metro->id;
		}
		else
		{
			return null;
		}
	}
}