<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{

});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('/login');
		}
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('editable_course', function($route) {
	$access = false;
	$course = Course::whereSeoTitle($route->getParameter('title'))->first();
	$user = Auth::user();

	/* Если проинициализирован курс и есть школа у юзера,
	тогда заходим в проверку соответствия школы */
	if ($course && $user->school) {
		$access = $course->school->id == $user->school->id;
	}
	// Иначе проверяем права юзера (админ/суперадмин)
	else {
		$roles = Auth::user()->roles;
		foreach($roles as $role) {
			if ($role->id == 1 || $role->id == 2) {
				$access = true;
				break;
			}
		}
	}

	if (!$access) {
		return View::make('pages.message', [
			'messageTitle' => 'Ошибка доступа',
			'messageBody' => 'У Вас не хватает прав для доступа к этой странице!'
		]);
	}
});

Route::filter('isset_course', function($route) {
	$course = Course::whereSeoTitle($route->getParameter('title'))->first();
	if (!$course) {
		if (Request::header('referer')) {
			return Redirect::back();
		}
		else {
			return Redirect::route('catalog');
		}
	}
	else {
		Data::set('course', $course);
	}
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
