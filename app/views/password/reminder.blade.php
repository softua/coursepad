@extends('...layouts.one-column')

@section('title')
:: Восттановление пароля
@stop

@section('content')
	<div class="modal-dialog" id="register_dialog">
	<div class="modal-content">
		<div class="modal-header">
			@if(Session::has('error'))
				<h1>{{ Session::get('error') }}</h1>
			@elseif(Session::has('status'))
				<h1>{{ Session::get('status') }}</h1>
			@endif
			<h5 class="modal-title">Востановление пароля</h5>
		</div>
		@include('includes.errors')
		@include('includes.alerts')
		<div class="modal-body">
			{{ Form::open(['action' => 'RemindersController@postRemind','class'=> 'form-vertical', 'method'=> 'post']) }}
				<div class="form-group">
					<div>
						{{ Form::email('email', null, ['placeholder'=> 'Введите email', 'required'=> 'required','id'=> 'email', 'class'=> 'input-xxlarge form-control check']) }}
						<p id="email_em_" style="display:none; color:red" class="help-block"></p>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						{{ Form::submit('Подтвердить',['id'=>'btn-register', 'class'=> 'input-xxlarge form-control btn btn-primary']) }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@stop