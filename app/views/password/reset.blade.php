@extends('layouts.one-column')

@section('title')
:: Вводим новый пароль
@stop

@section('content')
	<div class="modal-dialog" id="register_dialog">
	<div class="modal-content">
		<div class="modal-header">
			@if(Session::has('error'))
				<h1>{{ Session::get('error') }}</h1>
			@endif
			<h5 class="modal-title">Вводим новый пароль</h5>
		</div>
		@include('includes.errors')
		@include('includes.alerts')
		<div class="modal-body">
			{{ Form::open(['action' => 'RemindersController@postReset','class'=> 'form-vertical', 'method'=> 'post']) }}
				<div class="form-group">
					<div>
						{{ Form::email('email', null, ['placeholder'=> 'Введите email', 'required'=> 'required','id'=> 'email', 'class'=> 'input-xxlarge form-control check']) }}
						{{ Form::hidden('token', $token) }}
						<p id="email_em_" style="display:none; color:red" class="help-block"></p>
					</div>
				</div>
				<div class="form-group">
						<div>
							{{ Form::password('password', ['placeholder'=> 'Введите пароль', 'required'=> 'required', 'id'=> 'password-reg', 'class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
							<p id="password_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::password('password_confirmation', ['placeholder'=> 'Повторите пароль', 'required'=> 'required', 'id'=> 'password_confirmation','class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
							<p id="pasword_comfirmation_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
				<div class="form-group">
					<div class="controls">
						{{ Form::submit('Заменить',['id'=>'btn-register', 'class'=> 'input-xxlarge form-control btn btn-primary']) }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@stop