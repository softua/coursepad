@extends('layouts.one-column')

@section('title')
:: {{ $school->title }}
@stop

@section('sidebar')
	@include('includes.main-categories')
@stop

@section('content')

@include('includes.filters')

<div class="tab-content" data-ajax="container"></div>
<div class="ajax-content">
	@if(isset($courses) && count($courses))
    <div class="col-sm-12 text-center">{{ $courses->links() }}</div>
	@foreach($courses as $course)
		<div class="col-lg-6 ">
			<div class="course-content">
			<div class="col-sm-12">
			@if($editable)
                <a href="{{ URL::route('course-edit', [$course->seo_title]) }}" type="button" class="btn btn-primary btn-xs edit-course"><span class="glyphicon glyphicon-pencil"></span></a>
                @endif
                <div class="card-wrap">
                    <div class="card">
                        @if($course->images)
                            <a target="_blank" href="" class="photo">
                                <img src="/uploads/course/{{ $course->first_image }}" alt="{{ $course->title }}">
                            </a>
                        @endif
                        <br>
                        <button type="button" class="btn btn-success">Записаться</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="info">
                        @if($course->groups && !$course->groups->isEmpty())
                            <div class="price" >
                                <span class="value">{{ number_format($course->first_group->price, 2, '.', ' ') }}</span> <span id="rub-symbol">&nbsp;&nbsp;&#8381;</span>
                            </div>
                        @endif
                        <h3 class="name">
                            <a target="_blank" href="{{ URL::route('course', [$course->seo_title]) }}">{{ $course->title }}</a>
                        </h3>
                        <div class="subjects">
                            <a href="{{ URL::route('school', [$course->school->seo_title]) }}">{{ $course->school->title }}</a>
                        </div>
                        <div class="description">
                            <h3 class="title">Короткое описание</h3>
                            {{ $course->short_description }}
                        </div>
                        @if($course->groups && !$course->groups->isEmpty())
                            <div class="description">
                                <h3 class="title">Дата и время начала ближайшего курса</h3>
                                {{ date('d.m.Y H:i', strtotime($course->first_group->begins)) }}
                            </div>
                        @endif
                        @if($course->level)
                        <div class="description">
                            <h3 class="title">Уровень</h3>
                            {{ $course->level->level }}
                        </div>
                        @endif
                        @if($isAdmin)
                        <div class="description">
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-primary" data-color="primary">Активировать курс</button>
                                <input type="checkbox" class="hidden" name="status" value="1" />
                            </span>
                        </div>
                        @endif
                        <div class="groups-list">
                            @if($editable)
                                <table class="table">
                                    <tr>
                                        <th class="text-center">Время начала</th>
                                        <th class="text-center">Кол-во уроков</th>
                                        <th class="text-center">Кол-во мест</th>
                                        <th class="text-center">Цена</th>
                                        <th class="text-center">Редактирование</th>
                                    </tr>
                                    @if($course->groups)
                                    @foreach($course->groups()->get() as $group)
                                        <tr data-group-id="{{ $group->id }}" id="group_{{ $group->id }}">
                                            <td class="text-center changeable" input-name="date_start">{{ date('d.m.Y H:i', strtotime($group->begins)) }}</td>
                                            <td class="text-center changeable" input-name="lessons_count">{{ $group->lessons }}</td>
                                            <td class="text-center changeable" input-name="place_count">{{ $group->seats_all }}</td>
                                            <td class="text-center changeable" input-name="price">{{ number_format($group->price, 2, '.', ' ') }}</td>
                                            <td class="text-center group-controls">
                                                @if(!$group->seats_busy)
                                                    <span class="glyphicon glyphicon-remove del-defined-group" data-delete-href="{{ URL::action('AjaxController@deleteGroup', [$group->id]) }}"></span>
                                                @endif
                                                <span class="glyphicon glyphicon-pencil ok-defined-group" id="edit_control_group_{{ $group->id }}" data-edit-href="{{ URL::action('AjaxController@editGroup', [$group->id]) }}"></span>
                                                <span class="glyphicon glyphicon-ok save-defined-group"></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </div>

                        <div class="form-group">
                            <div>
                                <div class="groups-target"></div>
                                <div class="groups-data-target"></div>
                                <span class="add-group-control btn btn-primary" data-course-id="{{ $course->id }}" data-add-href="/ajax/group/add/{{ $course->id }}">Добавить группу<span class="glyphicon glyphicon-plus"></span></span>
                            </div>
                        </div>
                        @endif
                        <hr>
                </div>
                <div class="meta">
                    <div class="place">
                        <span class="icons icon-place"></span> {{ $course->metro }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
	</div>



	@endforeach
	@endif

        <div class="col-sm-12 text-center">{{ $courses->links() }}</div>
    <div class="col-sm-12">{{ $school->short_description }}</div>


</div>
@stop
@section('scripts')
	<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
	<script src="/js/jquery.datetimepicker.js" type="text/javascript"></script>
	<script src="/js/add-group.constructor.js" type="text/javascript"></script>
	<script src="/js/checkboxes.style.js" type="text/javascript"></script>
	<script type="text/javascript" src="/js/catalog.js"></script>
@stop