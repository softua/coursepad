@extends('layouts.one-column')

@section('title')
:: Регистрация
@stop

@section('content')
	<div class="modal-dialog " id="register_dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Регистрация школы</h5>
			</div>
			@include('includes.errors')
			@include('includes.alerts')
			<div class="modal-body">
				{{ Form::open(['id'=> 'register', 'class'=> 'form-vertical', 'method'=> 'post', 'action'=> 'UserController@postRegister']) }}
					{{ Form::text('image_id', null, ['class'=> 'image_id','hidden']) }}
					<div class="form-group">
						<div>
							{{ Form::text('title', null, ['placeholder'=> 'Название школы', 'id'=>'title', 'class'=> 'input-xxlarge form-control check']) }}
							<p id="contact_name_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::email('email', null, ['placeholder'=> 'Введите email', 'required'=> 'required','id'=> 'email', 'class'=> 'input-xxlarge form-control check']) }}
							<p id="email_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::password('password', ['placeholder'=> 'Введите пароль', 'required'=> 'required', 'id'=> 'password-reg', 'class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
							<p id="password_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::password('password_confirmation', ['placeholder'=> 'Повторите пароль', 'required'=> 'required', 'id'=> 'password_confirmation','class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
							<p id="pasword_comfirmation_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::text('contact_name', null, ['placeholder'=> 'Введите ФИО', 'id'=>'name', 'class'=> 'input-xxlarge form-control check']) }}
							<p id="contact_name_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::text('contact_phone', null, ['placeholder'=> 'Введите номер телефона', 'id'=> 'phone','class'=> 'input-xxlarge form-control check']) }}
							<p id="contact_phone_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div>
							{{ Form::textarea('short_description', null, ['class'=> 'input-xxlarge form-control', 'placeholder'=> 'Короткое описание']) }}
							<p id="short_description_em_" style="display:none; color:red" class="help-block"></p>
						</div>
					</div>
					<div class="form-group">
						<div style="margin:10px 0px">
							<span class="btn btn-default upload-btn js-fileapi-wrapper" style="position:relative; margin:1px;cursor:pointer;">
								<div class="upload-btn__txt">Выбрать картинку</div>
								{{ Form::file('img_url', ['id'=> 'choose']) }}
							</span>
							<div id="images" style="text-align: center;margin: 3% auto;"><!-- previews --></div>
						</div>
						<script>
							window.FileAPI = { staticPath: '/js/FileAPI/dist/' };
						</script>
						<script src="/js/FileAPI/dist/FileAPI.min.js"></script>
					</div>
					<div class="form-group">
						<div class="controls">
							{{ Form::submit('Зарегистрировать', ['id'=>'btn-register', 'class'=> 'input-xxlarge form-control btn btn-primary']) }}
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script src="/js/school-registration.js"></script>
	<!-- такой же класс используется на форме курса -->
	<style>
		.upload-btn input {
			top: 0;
			left: 0;
			font-size: 25px;
			cursor:pointer;
		}
	</style>
@stop
