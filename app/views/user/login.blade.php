@extends('layouts.one-column')

@section('title')
:: Вход
@stop

@section('content')
<div class="modal-dialog" id="register_dialog">
	<div class="modal-content">
		<div class="modal-header">
			@if(isset($message) && $message)
				<h1 class="error">{{ $message }}</h1>
			@endif
			<h5 class="modal-title">Вход</h5>
		</div>
		@include('includes.errors')
		@include('includes.alerts')
		<div class="modal-body">
			{{ Form::open(['action' => 'UserController@login','class'=> 'form-vertical', 'method'=> 'post']) }}
				<div class="form-group">
					<div>
						{{ Form::email('email', null, ['placeholder'=> 'Введите email', 'required'=> 'required','id'=> 'email', 'class'=> 'input-xxlarge form-control check']) }}
						<p id="email_em_" style="display:none; color:red" class="help-block"></p>
					</div>
				</div>
				<div class="form-group">
					<div>
						{{ Form::password('password', ['placeholder'=> 'Введите пароль', 'required'=> 'required', 'id'=> 'password-reg', 'class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
						<p id="password_em_" style="display:none; color:red" class="help-block"></p>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						{{ Form::submit('Войти',['id'=>'btn-register', 'class'=> 'input-xxlarge form-control btn btn-primary']) }}
					</div>
				</div>
			{{ Form::close() }}
			У вас нет аккаунта? <a href="{{ URL::action('UserController@register') }}">Регистрация</a>
		</div>
	</div>
</div>
@stop