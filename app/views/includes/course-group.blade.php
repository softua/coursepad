<div class="course-group-closest-block">
    <div class="course-group-closest-date-lessons">
        {{ $group->present()->courseBegins }} <span>{{ $group->course->present()->lessons }}</span>
    </div>

    <div class="pull-left">
        <a href="{{ URL::route('buy', [$group->id]) }}" class="btn btn-success">Записаться</a>
    </div>
    <div class="course-group-closest-order-price pull-left">{{ $group->price }} <i class="fa fa-rouble"></i></div>
    <div class="course-group-closest-places-block pull-left">
        <div class="course-group-closest-places">
            Осталось <strong>{{ $group->seats_all }} {{ $group->present()->seatsLeft }}</strong>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
