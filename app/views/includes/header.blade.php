<div style="z-index:2061" class="modal fade bs-example-modal-sm ulogin" tabindex="-1" role="dialog" aria-labelledby="ulogin" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div style="border-radius:10px" class="modal-content">
			<button class="close" type="button" data-dismiss="modal">x</button>
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Войти</h4>
			</div>
			<div class="modal-body">
				{{ Form::open(['id'=>'ulogin', 'class'=> 'bs-example bs-example-form form-vertical']) }}
					<div class="input-group input-group-lg">
						<span class="input-group-addon">@</span>
						{{ Form::email('email', null, ['placeholder'=> 'Введите email', 'autofocus','required'=> 'required','id'=> 'uloginemail', 'class'=> 'input-xxlarge form-control check']) }}
					</div>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						{{ Form::password('password', ['placeholder'=> 'Введите пароль', 'required'=> 'required', 'id'=> 'uloginpassword', 'class'=> 'input-xxlarge form-control', 'maxLength'=> '20']) }}
					</div>
					<p>
					<a href="{{ URL::action('RemindersController@getRemind') }}">Забыли свой пароль?</a></p>
					У вас нет аккаунта? <a href="{{ URL::action('UserController@register') }}">Регистрация</a>
					<input type="submit" style="display:none" />
				{{ Form::close() }}
				<p id="contact_name_em_" style="display:none; color:red" class="help-block">вход не выполнен</p>
			</div>
			<div class="modal-footer">
				<button id="ulogin-submit" class="btn btn-primary" type="button">Вход</button>
			</div>
		</div>
	</div>
</div>
<div id="header" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="navbar-header">
		<div class="logo">
			<a class="navbar-brand" href="/">CoursePad.ru</a>
		</div>
		<div class="currentCity">Курсы в <a href="#cityModal" role="button" data-toggle="modal">Москве</a>
		</div>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-top-menu">
			<span class="sr-only">Показать меню</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="collapse navbar-collapse" id="main-top-menu">
		<ul class="nav navbar-nav" id="top-menu">
			<li class="teachers active"><a href="/catalog"><span>Курсы <span class="label label-default">615</span></span></a></li>
			<li><a href="#"><span>Блог</span></a></li>
			<li><a href="#"><span>О проекте</span></a></li>
			<li><a href="#"><span>Частые вопросы</span></a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right" id="top-user-menu">
			@if(Auth::check())
				<li class="dropdown text-center">
					@if(Auth::user()->school)
					<a data-toggle="dropdown" href="#">{{ Auth::user()->school->title }}</a>
					@else
					<a data-toggle="dropdown" href="#">{{ Auth::user()->contact_name }}</a>
					@endif
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						@foreach(Auth::user()->roles()->get() as $role)
							@if($role->id == 3)
							<li role="presentation">
								<a role="menuitem" tabindex="-1" href="{{ URL::route('school', [Auth::user()->school->seo_title]) }}">Мои курсы</a>
							</li>
							@endif
						@endforeach
						<li role="presentation">
							<a href="{{ URL::route('course-add') }}">Добавить курс</a>
						</li>
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="{{ URL::route('logout') }}">Выход</a>
						</li>
					</ul>
				</li>
			@else
				<li class="user-login-logout-control"><a class="sign-in-link" role="button" data-toggle="modal" data-target=".ulogin">Войти</a></li>
				<li class="create-course"><a href="{{ URL::route('register') }}" class="btn btn-success">Разместить курс</a></li>
			@endif
		</ul>
	</div>
</div>