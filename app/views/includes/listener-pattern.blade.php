<div class="row listener-pattern">
    <div class="col-lg-3 col-md-3">{{ Form::label('listenerFirstName-{%listenerId%}', 'Участник №{%listenerId%}', ['class' => 'listenerLabel listenerNumberNamePattern']) }}</div>
    <div class="col-lg-4 col-md-4">
        <!-- Listener First name Form Input -->
        <div class="form-group">
            {{ Form::label('listenerFirstName-{%listenerId%}', 'Имя участника', ['class' => 'sr-only listenerLabel']) }}
            {{ Form::text(
                'listeners[{%listenerId%}][first_name]',
                NULL,
                [
                    'id'          => 'listenerFirstName-{%listenerId%}',
                    'class'       => 'form-control',
                    'placeholder' => 'Имя участника',
                    'disabled' => true,
                ]) }}
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <!-- Listener Last name Form Input -->
        <div class="form-group">
            {{ Form::label('listenerLastName-{%listenerId%}', 'Фамилия участника', ['class' => 'sr-only listenerLabel']) }}
            {{ Form::text(
                'listeners[{%listenerId%}][last_name]',
                NULL,
                [
                    'id'          => 'listenerLastName-{%listenerId%}',
                    'class'       => 'form-control',
                    'placeholder' => 'Фамилия участника',
                    'disabled' => true,
                ]) }}
        </div>
    </div>
    <div class="col-xs-1">
        <a href="#" class="btn btn-danger delete-listener-btn"><i class="fa fa-trash"></i></a>
    </div>
</div>