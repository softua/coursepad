<div id="footer">
	<div id="footer-menu">
		<div class="main-wrapper">
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav footer-menu">
					<li class="active"><a href="#">Как это работает</a></li>
					<li><a href="#">Помощь</a></li>
					<li><a href="#">Репетиторам</a></li>
					<li><a href="#">Блог</a></li>
					<li><a href="#">Частые вопросы</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right footer-menu-right">
					<li>
						<p>
							Поиск репетиторов в <a href="#cityModal">Москве</a>
						</p>
					</li>
					<li class="languages-menu"><a data-toggle="dropdown" href="#" class="dropdown-toggle">Выберите
						язык</a>
						<ul class="dropdown-menu dropdown-navbar submenu">
							<li><a class="active" href="">Русский</a></li>
							<li><a href="">Українська</a></li>
							<li><a href="">English</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-wrapper row">
		<div class="col-md-6 social">
			<h4 class="title">FreeTeachers.ru в социальных сетях</h4>

			<div class="social-links">
				<a class="vk" href="http://vk.com"><span
						class="icons icon-vk"></span> Вконтакте</a>
				<a class="tw" href="http://twitter.com"><span
						class="icons icon-tw"></span>Твиттер</a>
				<a class="fb" href="http://facebook.com"><span
						class="icons icon-fb"></span>Фейсбук</a>
			</div>
			<div class="social-share">
				<div class="fb">
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffreeteachers.ru&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21&amp;appId=1374777549434381"></iframe>
				</div>
				<div class="plusone">
					<div class="g-plusone" data-size="tall" data-annotation="inline" data-width="120"></div>
				</div>
				<div id="vk_like"></div>
				<div class="tweet">
					<a href="https://twitter.com/share" class="twitter-share-button"
					data-url="http://freeteachers.ru/">Tweet</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 contacts">
			<div class="address">г. Москва, ул. Большая Серпуховская, д. 44 оф. 19</div>
			<a class="email" href="mailto:info@freeteachers.ru">info@freeteachers.ru</a>

			<div class="phone">+7 999 999-99-99</div>

			<p class="copyright">
				&copy; 2013-2014 Freeteachers
			</p>
		</div>
	</div>