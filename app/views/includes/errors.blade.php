@if($errors->all())
	<div class="alert alert-danger fade in" role="alert">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	@foreach($errors->all() as $error)
        <strong>{{ $error }}</strong>
    @endforeach
	</div>
@elseif(Session::has('errors'))
	{{ var_dump(Session::get('errors')) }}
@endif