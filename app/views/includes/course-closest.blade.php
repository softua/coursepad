<div class="course-group-closest @if($withBorder) course-group-closest-with-border visible-lg-block @endif">
    <div class="course-group-closest-label">Ближайший курс</div>
    <div class="course-group-closest-date">{{ $closestGroup->present()->courseBegins }}</div>
    <div class="course-group-closest-price"><span>{{ $closestGroup->price }}</span> <i class="fa fa-rouble"></i></div>
    <div class="course-group-closest-order-btn">
        <a href="{{ URL::route('buy', [$closestGroup->id]) }}" class="btn btn-success btn-lg">Записаться на курс</a>
    </div>
    <div class="course-group-closest-places-block">
        <div class="course-group-closest-places">
            Осталось <br/>
            <strong>{{ $closestGroup->seats_all }} {{ $closestGroup->present()->seatsLeft }}</strong>
        </div>
    </div>
    <div class="clearfix"></div>
</div>