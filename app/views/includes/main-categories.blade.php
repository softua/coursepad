<div id="submenu">
	<ul id="categories">
	@foreach($mainCats as $cat)
<li @if(isset($category) and $category and $cat->id == $category->id) class="active" @endif>
<a href="{{ URL::route('catalog', [$cat->seo_title])  }}">{{ $cat->title }}</a>
</li>
	@endforeach
	</ul>
</div>
@if(isset($subCategories) and $subCategories)
	@include('includes.sub-categories')
@endif