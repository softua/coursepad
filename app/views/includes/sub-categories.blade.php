<div id="subcategory">
    <div id="categoryInfo">
        <div class="category-title">
            <h2>{{ $category->title }}</h2>
        </div>
        <div class="category-courses">
            {{ $courses->getTotal() }} @choice('курс|курса|курсов', $courses->getTotal(), array(), 'ru')
        </div>
    </div>
    <div id="subCategories">
        <ul>
        <?php $i=0;?>
        @foreach($subCategories as $subCategory)
            <li @if($subCategory->id == $activeCategory->id) class="active" @endif><a href="{{ URL::route('catalog', [$category->seo_title, $subCategory->seo_title])  }}">{{ $subCategory->title }}</a></li>
            @if($i >= 3)
            </ul><ul>

            <?php $i=0?>
            @else
            <?php $i++?>
            @endif
        @endforeach
        </ul>
    </div>
</div>
