<div class="comment">
    <div class="col-sm-3 text-center"><img src="/img/avatar-stub.png" alt=""/></div>
    <div class="col-sm-9">
        <div class="comment-author">Светлана Еромина</div>
        <div class="comment-title-stars">
            <div class="comment-title">Классная акция! Пять баллов!</div>
            <div class="comment-stars">
                <div class="course-rating-stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>
        <div class="comment-description">
            <p>Позвонила в автошколу, проконсультировалась. Всё устроило. Буду учиться!</p>
            <p>Спасибо большое за такую акцию!</p>
        </div>
        <div class="comment-date">15.04.2014 13:33</div>
    </div>
</div>
