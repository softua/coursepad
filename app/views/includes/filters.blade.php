<p class="visible-xs" id="show-subject-menu">
        <button type="button" class="btn btn-primary" data-toggle="offcanvas">Выбрать предмет</button>
    </p>
<div id="filters" class="col-sm-12">
	<h1 class="title">Курсы по всем направлениям в Москве</h1>
	<form class="form-inline form-horizontal" action="" method="get">
		<div class="col-lg-2 col-md-3 col-sm-4">
			<div class="row">
	            <label for="inputAge" class="col-lg-4 col-sm-3 control-label">Возраст</label>
	            <div class="col-lg-8 col-sm-9">
	                {{ Form::select('age', $ages, $command->age, ['class' => 'form-control filter-select select-styled selectpicker']) }}
	            </div>
            </div>
        </div>

		<div class="col-lg-2 col-md-3 col-sm-4">
			<div class="row">
				<label for="inputLevel" class="col-lg-4 col-sm-3 control-label">Уровень</label>
                <div class="col-lg-8 col-sm-9">
                    {{ Form::select('level', $levels, $command->level, ['class' => 'form-control filter-select select-styled selectpicker']) }}
                </div>
			</div>
		</div>

		<div class="col-lg-2 col-md-3 col-sm-4">
			<div class="row">
				<label for="inputMetro" class="col-lg-4 col-sm-3 control-label">Метро</label>
                <div class="col-lg-8 col-sm-9">
                    {{ Form::select('metro', $metroList, NULL, ['class' => 'form-control filter-select selectpicker', 'data-live-search' => 'true']) }}
                </div>
			</div>
        </div>
		<div class="col-lg-4 col-md-4 col-sm-5 top-margin-10">
			<div class="row">
				<label for="inputPrice" class="control-label col-lg-3 col-sm-3 col-xs-2 inputPriceLabel">Стоимость</label>
				<div class="col-lg-1 col-xs-2 priceFrom text-right">{{ $command->price_from or $prices['price_from'] }}</div>
				<div class="col-sm-5 col-xs-6">
	                <input id="inputPrice" type="text" value="" data-slider-min="{{ $prices['price_from']}}" data-slider-max="{{ $prices['price_to'] }}" data-slider-step="100" data-slider-value="[{{ $command->price_from or $prices['price_from'] }},{{ $command->price_to or $prices['price_to'] }}]"/>
	                {{ Form::hidden('price_from', $command->price_from, ['id'=>'price_from']) }}
	                {{ Form::hidden('price_to', $command->price_to, ['id'=>'price_to']) }}
				</div>
				<div class="col-lg-1 col-xs-2 priceTo">{{ $command->price_to or $prices['price_to'] }}</div>
			</div>
		</div>
		<div class="col-lg-offset-1 col-md-offset-6 col-sm-offset-4 col-lg-1 col-md-2 col-sm-3 top-margin-10">
			<button type="submit" class="btn btn-primary btn-block">Найти</button>
		</div>
	</form>
</div>