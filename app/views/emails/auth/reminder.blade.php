<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title>Password Reset</title>
	</head>
	<body>
		<div id="page-body-full" class="row-offcanvas row-offcanvas-left">
			<div id="main">
				<div id="main">
				<div class="row">
					<div class="col-md-6 text-center">To reset your password, complete this form: {{ URL::to('password/reset', array($token)) }}.</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-center">This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.</div>
				</div>
			</div>
			</div>
		</div>
	</body>
</html>