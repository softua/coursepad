<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Успешная оплата</title>
</head>
<body>
    <h1>Ваши билеты на {{ $order->group->course->title }}</h1>
    <h2>Здравствуйте, {{ $payer->first_name }}</h2>
    <p>
        Вы только что зарегистрировались на курс "{{ $order->group->course->title }}" на сайте <a href="{{ URL::to('/') }}">{{ URL::to('/') }}</a>. Подтверждаем успешную оплату курса.
    </p>
    <h3>Информация о курсе:</h3>
    <table>
        <tbody>
            <tr>
                <td>Название курса</td>
                <td>{{ $order->group->course->title }}</td>
            </tr>
            <tr>
            	<td>Кол-во занятий</td>
            	<td>{{ $order->group->lessons }}</td>
            </tr>
            <tr>
            	<td>Дата и время</td>
            	<td>{{ date_format(new DateTime($order->group->begins), 'd.m.Y H:i') }}</td>
            </tr>
            <tr>
                <td>Место проведения</td>
                <td>{{ preg_replace('/Россия, Москва, /', '', $order->group->course->location_addr) }}</td>
            </tr>
            <tr>
                <td>Зарегестрированные участники</td>
                <td>{{ implode(', ', $listeners) }}</td>
            </tr>
            <tr>
                <td>Телефон организатора</td>
                <td>{{ $order->group->course->school->user->contact_phone }}</td>
            </tr>
        </tbody>
    </table>
    <a href="{{ URL::to('/') }}">Ссылка на бронь</a>
</body>
</html>