<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<title>Активация аккаунта</title>
	</head>
	<body>
		<div id="page-body-full" class="row-offcanvas row-offcanvas-left">
			<div id="main">
				<div class="row">
					<div class="col-md-6 text-center">Для завершения регистрации пройдите по ссылке: <a href="{{ $activationUrl }}"><b>Активация</b></a></div>
				</div>
			</div>
		</div>
	</body>
</html>