@extends('layouts.one-column')

@section('title')
:: Оплата курса
@stop

@section('content')
	{{ Form::open(['url'=> $url, 'method'=> 'post']) }}
        {{ Form::hidden('MrchLogin', $MrchLogin) }}
        {{ Form::hidden('OutSum', $OutSum) }}
        {{ Form::hidden('InvId', $InvId) }}
        {{ Form::hidden('SignatureValue', $SignatureValue) }}
        {{ Form::submit('submit', ['id'=> 'submit']) }}
    {{ Form::close() }}

    <script>
        $(document).ready(function() {
            $('#submit').click();
        });
    </script>
@stop