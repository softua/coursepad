@extends('layouts.one-column')

@section('title')
:: Главная
@stop

@section('content')
<div id="banner">
	<div class="banner">
		<ul>
			<li class="row" data-transition="fade" data-slotamount="5" data-masterspeed="700">
				<img src="img/transparent.png" data-bgfit="cover" data-bgposition="center center"
						data-bgrepeat="no-repeat" alt="Alt" >

				<div class="title tp-caption sft start" data-x="100"
						data-y="0"
						data-speed="1500"
						data-start="100"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Linear.easeNone">
					<h1>Репетиторы по <span class="subject">Английскому языку</span></h1>

					<p>
						<b>Фритичерс</b> — Самая современная платформа для поиска<br/> индивидуальных преподавателей
						английского языка <br/><b class="home">на дому</b> и <b class="skype">по Скайпу</b></p>
				</div>
				<a class="btn btn-primary tp-caption sft start" href=""
					id="inline"
					data-x="100"
					data-y="310"
					data-speed="1500"
					data-start="100"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
						>Найти репетитора <span
						class="glyphicon glyphicon-arrow-right"></span></a>

				<div class="or tp-caption sft start" data-x="290"
						data-y="315"
						data-speed="1500"
						data-start="100"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Linear.easeNone">
					или
				</div>
				<a class="btn btn-default tp-caption sft start catalog"
					data-x="340"
					data-y="310"
					data-speed="1500"
					data-start="100"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					href="">Перейти в каталог</a>

				<div class="more tp-caption sfb start"
						data-x="340"
						data-y="360"
						data-speed="1500"
						data-start="1200"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Linear.easeNone"
						><span class="icons icon-arrow-up"></span><span class="text">Более 620 репетиторов<br/>по всем предметам</span>
				</div>
				<div class="women tp-caption sfr" data-x="650" style="z-index: 1;"
						data-y="10"
						data-speed="1500"
						data-start="800"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Linear.easeNone"></div>
				<div class="tp-caption sft warranty" style="z-index: 2;"
						data-x="900"
						data-y="225"
						data-speed="1500"
						data-start="1200"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Linear.easeNone"
						>
					<img src="img/cachback.png" alt='Alt'>
				</div>
			</li>
		</ul>
	</div>
	<div class="bottom">
		<div class="warranty">
			<p>
				Мы НАЙДЕМ преподавателя, по заданным вами
				критериям. <strong>Если не понравится — вернем деньги
				за первое занятие!</strong></p>
		</div>
	</div>
	</div>
	<div id="how-it-work">
	<div class="main-wrapper">
		<h2 class="title">Как происходит работа</h2>

		<div class="banners">
			<div class="banner banner-request">
				<h3 class="title">Вы оставляете заявку</h3>
			</div>
			<span class="icons icon-sm-arrow-right"></span>

			<div class="banner banner-deals">
				<h3 class="title">Репетиторы присылают вам свои предложения</h3>
			</div>
			<span class="icons icon-sm-arrow-right"></span>

			<div class="banner banner-choose">
				<h3 class="title">Вы выбираете репетитора</h3>
			</div>
			<span class="icons icon-sm-arrow-right"></span>

			<div class="banner banner-success">
				<h3 class="title">Успешно сдаете экоооооорыврарваявптапоооооозамены и поступаете в вуз. Или проходите собеседование на
					работе.</h3>
			</div>
		</div>
	</div>
	</div>
	<div id="features">
	<div class="main-wrapper row">
		<h2 class="title">Наши преимущества</h2>

		<div class="banners ">
			<div class="banner col-md-6 col-lg-6 banner-individual">
				<h3 class="title">Индивидуальные занятия</h3>

				<p>Самый быстрый и эффективный способ обучения.</p>
			</div>
			<div class="banner col-md-6 col-lg-6 banner-big-choose">
				<h3 class="title">Большой выбор преподавателей</h3>

				<p>Оставьте заявку и получите десятки откликов свободных репетиторов проживающих рядом с вами.</p>
			</div>
		</div>
	</div>
	</div>
	<div id="counter">
	<div class="main-wrapper">
		<a class="btn btn-primary" href="">Оставитьььььььььььььььььь заявку на подбор репетитора <span
				class="glyphicon glyphicon-arrow-right"></span></a>

		<div class="value">
			<span>4</span>
			<span>7</span>
			<span>6</span>
			<span>8</span>
		</div>
		<div class="banner">
			<span class="title">Учеников</span>

			<p>уже нашли репетиторов</p>
		</div>
	</div>
	</div>
	<div id="discipline">
	<div class="row main-wrapper">
		<h2 class="title">Популярные дисциплины</h2>

		<div class="row">
			<div class="col-md-6">
				<ul class="list">
					<li role="presentation" class="header">Языки</li>
					<li>
						<a href="">Немецкий</a>
					</li>

					<li>
						<a href="">Французский</a>
					</li>

					<li>
						<a href="">Итальянский</a>
					</li>

				</ul>
				<a data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content="" href="" class="more">Все предметы</a>
			</div>
			<div class="col-md-6">
				<ul class="list">
					<li role="presentation" class="header">Предметы школы/вуза</li>
					<li>
						<a href="">Математика</a>
					</li>

					<li>
						<a href="">Физика</a>
					</li>

					<li>
						<a href="">Химия</a>
					</li>
				</ul>
				<a data-container="body" href="" data-toggle="popover" data-placement="top" data-html="true" data-content="" class="more">
					Все предметы</a>
			</div>
			<div class="col-md-6">
				<ul class="list">
					<li role="presentation" class="header">Спорт</li>
				</ul>
				<a data-container="body" data-toggle="popover" data-placement="top" data-html="true"
					data-content="" href="" class="more">
					Все предметы</a>
			</div>
		</div>
	</div>
	</div>
	<div id="top-teachers">
	<div class="main-wrapper">
		<h2 class="title">Популярные преподаватели</h2>

		<div id="top-teachers-carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active">
					<div class="teacher">
						<div class="photo">
							<img class="img-circle" src="img/photo.jpg" alt='Alt' />
						</div>
						<div class="info">
							<a href="#" class="name">Наталья Леонидовна</a>

							<div class="subject">
								Репетитор по <a href="">Химии</a>
							</div>
							<div class="reviews">
								<span class="icons icon-review"></span> 0 отзывов
							</div>
						</div>
					</div>
					<div class="teacher">
						<div class="photo">
							<img class="img-circle" src="img/photo2.jpg" alt='Alt' />
						</div>
						<div class="info">
							<a href="" class="name">Ксения Игоревна</a>

							<div class="subject">
								Репетитор по <a href="">Английскому языку</a>
							</div>
							<div class="reviews">
								<span class="icons icon-review"></span> 0 отзывов
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="teacher">
						<div class="photo">
							<img class="img-circle" src="img/photo.jpg" alt='Alt' />
						</div>
						<div class="info">
							<a href="#" class="name">Наталья Леонидовна</a>

							<div class="subject">
								Репетитор по <a href="">Химии</a>
							</div>
							<div class="reviews">
								<span class="icons icon-review"></span> 0 отзывов
							</div>
						</div>
					</div>
					<div class="teacher">
						<div class="photo">
							<img class="img-circle" src="img/photo2.jpg" alt='Alt' />
						</div>
						<div class="info">
							<a href="" class="name">Ксения Игоревна</a>

							<div class="subject">
								Репетитор по <a href="">Английскому языку</a>
							</div>
							<div class="reviews">
								<span class="icons icon-review"></span> 0 отзывов
							</div>
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#top-teachers-carousel" role="button" data-slide="prev"><span
					class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#top-teachers-carousel" role="button" data-slide="next"><span
					class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		<div class="all">
			<a href="catalog.html">Посмотреть каталог репетиторов</a>
		</div>
	</div>
	</div>
	<div id="questions">
	<div class="main-wrapper row">
		<div class="col-md-7">
			<h2 class="title">Часто задаваемые вопросы</h2>

			<h3>Должен ли я платить за поиск преподавателя?</h3>

			<p>
				Нет. Поиск репетиторов и их подбор осуществляется бесплатно. Также это не увеличивает стоимость занятий
				для вас. Наше вознаграждение составляет в среднем всего 3-5% от заработка репетитора, что никак не
				увеличивает конечную стоимость занятий для ученика.
			</p>

			<h3>Все ли преподаватели, представленные в базе доступны и набирают учеников?</h3>

			<p>
				Почти все. 90% преподавателей доступны и набирают учеников. Бывает так, что репетитор уже занял все свое
				свободное время занятиями, но не сообщил нам об этом, поэтому его анкета все еще видна пользователям, а
				учеников он взять не может.
			</p>

			<h3>Могу ли я отказаться от подобранного репетитора или отменить занятия с репетитором, которого я выбрал(а)
				самостоятельно?</h3>

			<p>
				Да, конечно. Подавая заявку на подбор репетитора вы не несете никаких обязательств. Также, если вам
				чем-то не понравился преподаватель, можно оставить новую заявку на сайте на подбор другого репетитора.
			</p>
		</div>
		<div class="col-md-4 vk">
			<h2 class="title">Мы в «Вконтакте»</h2>

			<div id="vk_groups"></div>

		</div>
	</div>
	</div>
	</div>

	<div id="time">
		<div class="main-wrapper">
			<a class="btn btn-primary" href="">Оставить заявку на подбор репетитора <span class="glyphicon glyphicon-arrow-right"></span></a>
			<div class="banner">
				<h3 class="title">Мало времени для поиска?</h3>

				<p>Разместите заявку и свободные репетиторы сами предложат вам свои услуги!</p>
			</div>
		</div>
	</div>
@include('includes.footer')
@stop
@section('scripts')
	<script type="text/javascript" src="/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="/js/mainpage.js"></script>
@stop