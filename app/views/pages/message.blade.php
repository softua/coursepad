@extends('layouts.one-column')

@section('title')
:: {{ $messageTitle }}
@stop

@section('content')
<div class="row-fluid">
	<div class="col-md-4-offset-4">
		<h1>{{ $messageTitle }}</h1>
        <p>{{ $messageBody }}</p>
	</div>
</div>
@stop