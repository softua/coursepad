<!DOCTYPE HTML>
<html lang="ru">
@include('includes.head')
<body>
@include('includes.header')
@yield('submenu')
<div id="main" class="container-fluid @if(isset($subCategories) and $subCategories) with-subcategories @endif">
@include('includes.session_message')
@yield('content')
</div>
<div class="clearfix"></div>
<script src="/js/jquery-2.1.1.min.js"></script>
{{--<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>--}}
<script src="/js/auto.hide.alert.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-slider.min.js"></script>
<script src="/js/jquery.history.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/login.js"></script>
<script type="text/javascript" src="/js/holder.js"></script>
@yield('scripts')
</body>
</html>