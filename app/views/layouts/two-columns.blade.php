<!DOCTYPE HTML>
<html lang="ru">
@include('includes.head')
<body>
@include('includes.header')
<div id="page-body" class="row-offcanvas row-offcanvas-left">
<div id="sidebar" class="sidebar-offcanvas subcats_closed">
    <div id="sidebar-content">
        @yield('sidebar')
    </div>
</div>
<div id="main">
@include('includes.session_message')
@yield('content')
</div>
</div>
<div class="clearfix"></div>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script src="/js/jquery.history.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/login.js"></script>
@yield('scripts')
</body>
</html>