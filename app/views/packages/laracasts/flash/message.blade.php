@if (Session::has('flash_notification.message'))
    @if (Session::has('flash_notification.overlay'))
        @include('flash::modal', ['modalClass' => 'flash-modal', 'title' => Session::get('flash_notification.title'), 'body' => Session::get('flash_notification.message')])
    @else
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            @if(is_array(Session::get('flash_notification.message')))
            <ul>
                @foreach(Session::get('flash_notification.message') as $message)
                <li>{{ $message }}</li>
                @endforeach
            </ul>
            @else
                {{ Session::get('flash_notification.message') }}
            @endif
        </div>
    @endif
@endif
