<div class="course @if(isset($fw) and $fw) col-md-12 col-sm-4 @else col-lg-3 col-sm-4 @endif">
    <div class="course-content">
        <div class="course-picture">
            @if($course->images)
                <a target="_blank" href="{{ URL::route('course', [$course->seo_title]) }}" class="photo">
                    <img src="/uploads/course/{{ $course->first_image }}" alt="{{ $course->title }}">
                </a>
            @else
                <a href="{{ URL::route('course', [$course->seo_title]) }}">
                    <img data-src="holder.js/100%x200">
                </a>
            @endif
        </div>
        @include('includes.level-age', $course)
        <div class="course-title">
            <h3>{{ $course->seo_title }}</h3>
        </div>
        <div class="course-rating">
            <div class="course-rating-messages pull-right">
                <i class="fa fa-comment-o"></i> 9 Отзывов
            </div>
            <div class="course-rating-label">Рейтинг курса</div>
            <div class="course-rating-stars">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="course-description">
            {{ str_limit($course->short_description, 250, '...') }}
        </div>
        <div class="course-group">
            <div class="course-group-label">Ближайший курс</div>
            <div class="course-group-date">{{ $course->first_group->present()->courseBegins }}</div>
        </div>
        <div class="course-price">
            <div class="course-order pull-left"><a href="{{ URL::route('buy', [$course->first_group->id]) }}" class="btn btn-success">Записаться</a></div>
            <div class="course-price-amount pull-right"><span>{{ number_format($course->first_group->price, 0, '.', ' ') }}</span> <i class="fa fa-rouble"></i></div>
            <div class="clearfix"></div>
        </div>
        <div class="course-more-btn">
            <a href="{{ URL::route('course', [$course->seo_title]) }}">Подробнее о курсе</a>
        </div>
    </div>
</div>