@extends('layouts.one-column')

@section('title')
:: Подписка на курс
@stop

@section('content')
<div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2" id="group-form">
	{{ Form::open(['route' => 'course-buy']) }}
	<div class="col-lg-12">
		<h1>Записаться на курс</h1>
		<h4>{{ $course->title }} @include('includes.rating')</h4>
		@include('includes.level-age', $course)
		<hr/>
	</div>
	<div class="col-lg-12">
		@include('flash::message')
	</div>
	<div class="col-lg-12">
		<h3>Информация о плательщике</h3>
		<div class="row">
			<div class="col-sm-6">
	            <!-- First_name Form Input -->
	            <div class="form-group">
	                {{ Form::label('first_name', 'Ваше имя') }}
	                {{ Form::text(
	                    'first_name',
	                    NULL,
	                    [
	                        'id'          => 'first_name',
	                        'class'       => 'form-control',
	                        'placeholder' => 'Укажите Ваше имя',
	                        'required'    => true,
	                    ]) }}
	            </div>
	        </div>
	        <div class="col-sm-6">
	            <!-- Last Name Form Input -->
	            <div class="form-group">
	                {{ Form::label('last_name', 'Ваша фамилия') }}
	                {{ Form::text(
	                    'last_name',
	                    NULL,
	                    [
	                        'id'          => 'last_name',
	                        'class'       => 'form-control',
	                        'placeholder' => 'Укажите Вашу фамилию',
	                        'required'    => true,
	                    ]) }}
	            </div>
	        </div>
		</div>
		<div class="row">
			<div class="col-sm-6">
                <!-- Phone Form Input -->
                <div class="form-group">
                    {{ Form::label('phone', 'Телефон') }}
                    {{ Form::text(
                        'phone',
                        NULL,
                        [
                            'id'          => 'phone',
                            'class'       => 'form-control',
                            'placeholder' => '',
                            'required'    => true,
                        ]) }}
                </div>
            </div>
            <div class="col-sm-6">
                <!-- Email Form Input -->
                <div class="form-group">
                    {{ Form::label('email', 'E-mail') }}
                    {{ Form::email(
                        'email',
                        NULL,
                        [
                            'id'          => 'email',
                            'class'       => 'form-control',
                            'placeholder' => 'Укажите Ваш контактный E-mail',
                            'required'    => true,
                        ]) }}
                </div>
            </div>
		</div>
		<hr/>
	</div>
	<div class="col-lg-12">
		<h3>Информация о курсе</h3>
		<div class="row">
			<div class="col-md-5 col-xs-7">
				<!-- Group Form Input -->
				<div class="form-group">
					{{ Form::label('group', 'Дата проведения' ) }}
					{{ Form::select(
						'group',
						$groupsSelect,
						$group->id,
						[
							'id'          => 'group',
							'class'       => 'form-control',
							'placeholder' => 'Укажите дату проведения'
						]) }}
				</div>
			</div>
			<div class="col-md-7 col-xs-5">
				<div class="course-group-closest-places">
                    Осталось <br/>
                    <strong>{{ $group->seats_all }} {{ $group->present()->seatsLeft }}</strong>
                </div>
			</div>
		</div>
		<hr/>
	</div>
	<div class="col-lg-12">
		<h3>Кто пойдёт на курс</h3>
		<div class="row">
			<div class="col-xs-12">
				<div class="checkbox">
					<label>
						{{ Form::checkbox('me_will_go', 1, false, ['id' => 'me_will_go']) }} Я пойду на курс
					</label>
				</div>
			</div>
		</div>
		<div id="listeners">
			@include('includes.listener-pattern')
		</div>
		<div class="row">
			<div class="col-lg-12">
				<a href="#" class="addListener-btn"><i class="fa fa-user"></i> Добавить ещё участника</a>
			</div>
		</div>
		<hr/>
	</div>
	<div class="col-lg-12">
		<h3>Общая стоимость</h3>
		<div class="row course-buy-data">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 text-center">
				<div class="course-group-closest-price">
                    <span>{{ $group->price }}</span> <i class="fa fa-rouble"></i>
                </div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
				<div class="course-group-closest-places-block">
                    <div class="course-group-closest-places">
                        <span class="data-group-listeners">1 участник</span> х <span class="data-group-price">{{ $group->price }}</span><br/>
                        <span>{{ $course->present()->lessons }} в курсе / время 1 занятия {{ $course->present()->duration }}</span>
                    </div>
                </div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		{{ Form::button('Отправить заявку', ['class' => 'btn btn-success btn-lg', 'type' => 'submit', ]) }}
	</div>
	{{ Form::close() }}
</div>
{{--<div class="modal-dialog" id="register_dialog">--}}
	{{--<div class="modal-content">--}}
		{{--<div class="modal-header">--}}
			{{--<h4 class="modal-title">Запись на курс</h4>--}}
			{{--<p>* - поля обязательные для заполнения</p>--}}
		{{--</div>--}}
		{{--<div class="modal-body">--}}
			{{--<form class="form-inline" role="form" method="POST" action="/course/buy/{{ $group->id }}">--}}
				{{--<h4 class="text-primary">Информация о платильщике</h4>--}}
				{{--<div class="form-group allwidth" >--}}
					{{--<div class="input-group allwidth" >--}}
						{{--<input type="text" name="contact_name" placeholder="ФИО *" id="name" class='input-xxlarge form-control check contact_name' />--}}
						{{--<p id="contact_name_em_" style="display:none; color:red" class="help-block"></p>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group allwidth" >--}}
					{{--<div class="input-group allwidth" >--}}
						{{--<input type="email" name="contact_email" placeholder="email *" id="email" class='input-xxlarge form-control check' />--}}
						{{--<p id="email_em_" style="display:none; color:red" class="help-block"></p>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group allwidth" >--}}
					{{--<div class="input-group allwidth" >--}}
						{{--<input type="tel" name="contact_tel" placeholder="tel. *" id="tel" class='input-xxlarge form-control check' />--}}
						{{--<p id="email_em_" style="display:none; color:red" class="help-block"></p>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<hr>--}}
				{{--<h4 class="text-primary">Информация курса</h4>--}}
				{{--<div class="form-group" style="width:69%">--}}
					{{--<select name="id_group" class="id_group form-control" id="group" style="width:100%">--}}
					{{--@if(isset($groups) && $groups)--}}
						{{--@foreach($groups as $item)--}}
							{{--@if($item->id == $group->id)--}}
								{{--<option value="{{ $item->id }}" id="id_group_{{ $item->id }}" data-group-price="{{ $item->price }}" selected data-group-seatsFree="{{ $item->seats_all - $item->seats_busy }}">{{ date_format(new DateTime($item->begins), 'd.m.Y H:i') }}</option>--}}
							{{--@else--}}
								{{--<option value="{{ $item->id }}" id="id_group_{{ $item->id }}" data-group-price="{{ $item->price }}" data-group-seatsFree="{{ $item->seats_all - $item->seats_busy }}">{{ date_format(new DateTime($item->begins), 'd.m.Y H:i') }}</option>--}}
							{{--@endif--}}
						{{--@endforeach--}}
					{{--@endif--}}
					{{--</select>--}}
				{{--</div>--}}
				{{--<div class="form-group" style="width:30%">--}}
					{{--<select name="count_listeners" class="count_place form-control" style="width:100%">--}}
					{{--@for($i = 0; $i < $group->present()->seatsLeft; $i++)--}}
						{{--@if($i === 0)--}}
							{{--<option value="{{ $i + 1 }}" selected>{{ $i + 1 }}</option>--}}
						{{--@else--}}
							{{--<option value="{{ $i + 1 }}">{{ $i + 1 }}</option>--}}
						{{--@endif--}}
					{{--@endfor--}}
					{{--</select>--}}
				{{--</div>--}}
				{{--<div class="form-group allwidth" style="padding-top:30px">--}}
					{{--<span class="text-muted">Ценна за группу:&nbsp;&nbsp;<span class="text-success group_price">0</span><span id="rub-symbol">&nbsp;&nbsp;&#8381;</span></span>--}}
				{{--</div>--}}
				{{--<div class="form-group allwidth">--}}
					{{--<span class="text-muted">Вся сумма:&nbsp;&nbsp;<span class="text-success full_price">0</span><span id="rub-symbol">&nbsp;&nbsp;&#8381;</span></span>--}}
				{{--</div>--}}
				{{--<hr>--}}
				{{--<h4 class="text-primary">Слушатели курса</h4>--}}
				{{--<div class="form-group allwidth" >--}}
					{{--<div class="input-group allwidth" >--}}
						{{--<input type="button" class="btn btn-default add-listener" value="Сделать меня слушателем">--}}
						{{--<hr>--}}
						{{--<p id="listener_error" style="display:none; color:red" class="help-block"></p>--}}
						{{--<div class="input-group allwidth listener-container">--}}
							{{--<input type="text" name="listener[]" placeholder="ФИО *" class='input-xxlarge form-control check-listener' />--}}
						{{--</div>--}}
						{{--<hr>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group allwidth" >--}}
					{{--<div class="controls input-group allwidth" >--}}
						{{--<input type="submit" class='btn btn-primary form-control' id="btn-register" value="Подтвердить" />--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</form>--}}
		{{--</div>--}}
	{{--</div>--}}
{{--</div>--}}
@stop
@section('scripts')
<script type="text/javascript">
var groupsData = {{ json_encode($groupsData) }};
</script>
<script src="/js/JSON.js"></script>
<script src="/js/course-buy.js"></script>
@stop