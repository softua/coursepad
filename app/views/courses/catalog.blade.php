@extends('layouts.one-column')

@section('title')
	@if(isset($category) && $category)
		:: {{ $category->title }}
	@else
		:: Каталог курсов
	@endif
@stop

@section('submenu')
	@include('includes.main-categories')
@stop

@section('content')

@include('includes.filters')

<div class="tab-content" data-ajax="container"></div>

<div class="ajax-content">
	<div id="courses-list" class="container-fluid">
	@foreach($courses as $course)
		@include('courses.course', $course)
	@endforeach
	</div>

		@if(isset($category) && $category)
			<p>{{ $category->description }}</p>
		@endif
	<div class="container-fluid text-center">
		{{ $courses->links() }}
	</div>
</div>
<div class="col-sm-12">

<h3>Почему стоит выбрать индивидуальные занятия?</h3>

<p>Ваш преподаватель за час до урока пришлет Вам на электронную почту все учебные материалы, которые понадобятся Вам для занятий. Это также позволяет Вам позаниматься самостоятельно перед занятием. Затем Ваше занятие пройдет абсолютно также, как проходит обычный урок в классе. Вы можете попросить преподавателя обратить внимание на какие-то аспекты языка, которые Вам наиболее интересны, либо заниматься полностью по плану преподавателя.</p>

<p>В конце занятия преподаватель даст Вам небольшое домашнее задание, которое, в зависимости от типа упражнения, может быть размещено через один из форумов школы или отправлено по электронной почте преподавателю. Как только преподаватель получит выполненное задание, он уведомит Вас о получении, а к следующему занятию проверит задание.</p>
</div>
@stop

@section('scripts')
<script type="text/javascript" src="/js/catalog.js"></script>
@stop