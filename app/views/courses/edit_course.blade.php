@extends('layouts.one-column')

@section('title')
:: Редактирование "{{ $course->title }}"
@stop

@section('content')
	<div class="modal-dialog" id="register_dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Редактирование: {{ $course->title }}</h5>
			</div>
			@include('includes.errors')
			<div class="modal-body">
				{{ Form::open(['class'=> 'form-vertical add-course', 'method'=> 'post', 'action'=> ['SchoolController@postEditCourse', $course->seo_title]]) }}
					{{ Form::text('images', null, ['class'=> 'images','hidden']) }}
					<div class="form-group">
						<h3 class="text-label">
							{{ $course->school->title }}
						</h3>
					</div>
					<div class="form-group">
						<div>
							{{ Form::text('title', $course->title, ['placeholder'=> 'Название курса', 'id'=>'course_name', 'class'=> 'input-xxlarge form-control check']) }}
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div style="margin:10px 0px">
							<div class="btn btn-default upload-btn js-fileapi-wrapper" style="margin:1px;">
								<div class="upload-btn__txt">Выбрать картинки</div>
								{{ Form::file('', ['id'=> 'choose', 'multiple']) }}
							</div>
							<div class="loader"><img src="/img/icons/loader-1.gif" /></div>
							<ul id="sortable">
								@if($course->images)
									@foreach($course->images as $image_url)
										<li class="ui-state-default" id="id-image_{{ $image_url }}">
											<img width="160px" height="160px" src="/uploads/course/{{ $image_url }}" alt="{{ $course->title }}"/>
										</li>
									@endforeach
								@endif
							</ul>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div>
							{{ Form::textarea('short_description', $course->short_description, ['class'=> 'input-xxlarge form-control course-short_description', 'placeholder'=> 'Короткое описание']) }}
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div>
							<label for="full_description" style="color: #777;font-size: 16px; margin-left: 13px; font-weight: normal;">Полное описание</label>
							{{ Form::textarea('full_description', $course->full_description, ['id'=> 'full_description','class'=> 'full_description input-xxlarge form-control', 'placeholder'=> 'Полное описание','rows'=> '5']) }}
						</div>
					</div>
					<hr>
					<div class="form-group">
						<input  class="input-xxlarge form-control search_address" name="address" placeholder="Адресс" style="margin:10px 0px" autocomplete="off" />
						{{ Form::text('location_address', $course->location_addr, ['hidden', 'id'=>'course_address']) }}
						{{ Form::text('location_metro', $course->location_metro, ['hidden', 'id'=>'metro_address']) }}
						<div id="map"></div>
					</div>
					<hr>
					<div class="form-group">
						<div>
							<select name="category_id" class="form-control filter-select select-styled selectpicker">
							@foreach($cats as $category)
								@if(count($category->childrens))
								<optgroup label="{{ $category->title }}">
								@foreach($category->childrens as $subcat)
								@if($subcat->id === $course->category->id)
									<option selected value="{{ $subcat->id }}">{{ $subcat->title }}</option>
								@else
									<option value="{{ $subcat->id }}">{{ $subcat->title }}</option>
								@endif
								@endforeach
								</optgroup>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<h6 class="text-center lead muted">Возраст</h6>
						@if(isset($ages) && $ages)
                        <div class="input-group">
                            @foreach($ages as $age)
                                @if($age->id == $course->age->id)
                                <label class="btn btn-default">
                                    <input type="radio" name="age_id" checked id="age_{{ $age->id }}" value="{{ $age->id }}"> {{ $age->age }}
                                </label>
                                @else
                                <label class="btn btn-default">
                                    <input type="radio" name="age_id" id="age_{{ $age->id }}" value="{{ $age->id }}"> {{ $age->age }}
                                </label>
                                @endif
                            @endforeach
                        </div>
                        @endif
                        <hr>
						<h6 class="text-center lead muted">Уровень</h6>
						@if(isset($levels) && $levels)
                        <div class="input-group">
                            @foreach($levels as $level)
                                @if($level->id == $course->level->id)
                                <label class="btn btn-default">
                                    <input type="radio" name="level_id" checked id="level_{{ $level->id }}" value="{{ $level->id }}"> {{ $level->level }}
                                </label>
                                @else
                                <label class="btn btn-default">
                                    <input type="radio" name="level_id" id="level_{{ $level->id }}" value="{{ $level->id }}"> {{ $level->level }}
                                </label>
                                @endif
                            @endforeach
                        </div>
                        @endif
					</div>
					<hr>
					<div class="form-group text-center">
						<div class="controls">
							{{ Form::submit('Сохранить курс', ['id'=>'btn-register', 'class'=> 'input-xxlarge form-control btn btn-primary form-submit']) }}
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop

@section('scripts')
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
	window.FileAPI = { staticPath: '/js/FileAPI/dist/' };
</script>
<script src="/js/FileAPI/dist/FileAPI.min.js" type="text/javascript"></script>
<script src="/js/typehead.js" type="text/javascript"></script>
<script src="/js/courseadd.js" type="text/javascript"></script>
@stop
