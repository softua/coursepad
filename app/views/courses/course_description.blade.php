@extends('layouts.one-column')

@section('title'):: Курс "{{ $course->title }}"@stop

@section('content')
<div class="col-md-1">
	<div class="back-to-catalog">
		<a href="/catalog" class="btn btn-default btn-block back-btn"><i class="fa fa-arrow-left"></i> Назад</a>
	</div>
</div>
<div class="col-md-8">
	<div id="course-block">
		<div class="row">
			<div class="col-lg-7">
                <div class="course-picture">
                    @if($course->images)
                        <a target="_blank" href="{{ URL::route('course', [$course->seo_title]) }}" class="photo">
                            <img src="/uploads/course/{{ $course->first_image }}" alt="{{ $course->title }}">
                        </a>
                    @else
                        <a href="{{ URL::route('course', [$course->seo_title]) }}">
                            <img data-src="holder.js/100%x360">
                        </a>
                    @endif
                </div>
                <div class="course-level-age">
                    <div class="course-age">{{ $course->age->age }}</div><div class="course-level">{{ $course->level->level }}</div>
                </div>
                <div class="course-title">
                    <h3>{{ $course->seo_title }}</h3>
                </div>
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                      <li><a href="/">Главная</a></li>
                      @if($course->category->parent)
                        <li><a href="{{ URL::route('catalog', [
                          'categoryTitle' => $course->category->parent->seo_title,
                           ])  }}">{{ $course->category->parent->title }}</a></li>
                        <li><a href="{{ URL::route('catalog', [
                          'categoryTitle' => $course->category->parent->seo_title,
                           'subCategoryTitle' => $course->category->seo_title
                           ])  }}">{{ $course->category->title }}</a></li>
                        @else
                        <li><a href="{{ URL::route('catalog', [$course->category->seo_title])  }}">{{ $course->category->title }}</a></li>
                        @endif
                      <li class="active">{{ $course->title }}</li>
                    </ol>
                </div>
                <div class="col-sm-12">
                    <h1>{{ $course->title }}</h1>
                </div>
                <div class="col-sm-12">
                    <div class="bottom-border">
                        {{ $course->full_description }}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="bottom-border">
                        photos
                    </div>
                </div>
                <div class="col-sm-12">
                    <h2>Организатор</h2>
                </div>

                <div class="col-sm-12">
                    <div class="bottom-border">
                        <div class="row">
                            <div class="col-sm-4">организатор пик</div>
                            <div class="col-sm-8">
                                <h4>{{ $course->school->title }}</h4>
                                {{ $course->school->short_description }}
                                <div class="course-school-more"><a href="#">Подробнее о "{{ $course->school->title }}"</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <h2>Как проехать</h2>
                </div>
                <div class="col-sm-12">
                    <div class="bottom-border">
                        <div class="row">
                            <div class="col-sm-6">
                                Адрес:<br />Санкт-Петербург,  Дворцовая набережная, 38
                            </div>
                            <div class="col-sm-6">
                                Телефон:<br/>+7 (812) 710-95-50
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <h2>Отзывы о курсах</h2>
                </div>

                <div class="col-sm-12">
                    <div class="course-rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    9 Отзывов
                </div>

                <div class="col-sm-12">
                    @include('includes.comment')
                    @include('includes.comment')
                    @include('includes.comment')
                    <div class="col-sm-9 col-sm-offset-3"><a href="#">Показать ещё</a></div>
                </div>
                <div class="col-sm-12">
                    <h2>Добавить отзыв о курсе</h2>
                </div>
                <div class="col-sm-12">
                    <div class="course-comment-form">
                        <div class="col-sm-3 text-center">
                            <img src="/img/avatar-stub.png" alt=""/>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group col-sm-6">
                                <input type="text" name="name" id="inputID" class="form-control" value="" title="" required="required" placeholder="Ваше имя" >
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="text" name="name" id="inputID" class="form-control" value="" title="" required="required" placeholder="Ваше впечатление от курса" >
                            </div>
                            <div class="form-group col-sm-12">
                                <textarea class="form-control" rows="3"  placeholder="Текст отзыва"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="col-sm-4">Поставте оценку</div>
                            <div class="col-sm-8">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                        <div class="col-sm-9 col-sm-offset-3"><a href="#" class="btn btn-success btn-lg">Оставить отзыв</a></div>
                    </div>
                </div>
                <div class="col-sm-12"><hr/></div>
                <div class="col-sm-12">
                    @include('includes.course-closest', ['withBorder' => true])
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="course-groups" class="col-lg-5">
                <div class="course-rating">
                    <div class="course-rating-label">Рейтинг курса</div>
                    <div class="course-rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
                @include('includes.course-closest', ['withBorder' => false])
                <div class="course-groups-block">
                    <div class="course-groups-block-label">Следующие курсы</div>
                    @foreach($groups as $group)
                    @include('includes.course-group', $group)
                    @endforeach
                </div>
                <div class="course-group-time">
                    * время курса ~ {{ $course->getTotalDuration() }} @choice('час|часа|часов', $course->getTotalDuration())
                </div>
                <div class="course-group-share">
                    Поделиться В F T О M
                </div>
            </div>
            <div class="col-lg-5 course-groups-chevron hidden-md hidden-sm hidden-xs"></div>
            <div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="col-md-3">
    <h1>Похожие курсы</h1>
    @foreach($commonCourses as $course)
    		@include('courses.course', ['course' => $course, 'fw' => true])
    	@endforeach
</div>
{{--<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />--}}
	{{--<div class="course row">--}}
		{{--<div class="card-wrap">--}}
			{{--<div class="card">--}}
				{{--@if($course->images)--}}
					{{--<a target="_blank" href="" class="photo">--}}
						{{--<img src="/uploads/course/{{ $course->first_image }}" alt=""/>--}}
					{{--</a>--}}
				{{--@endif--}}
				{{--<br>--}}
				{{--<button type="button" class="btn btn-success">Записаться</button>--}}
			{{--</div>--}}
			{{--<div class="clearfix"></div>--}}
		{{--</div>--}}
		{{--<div class="info">--}}
			{{--<div class="row">--}}
				{{--@if($course->groups && !$course->groups->isEmpty())--}}
				{{--<div class="price">--}}
					{{--<span class="value">{{ number_format($course->first_group->price, 2, '.', ' ') }}</span>--}}
				{{--</div>--}}
				{{--@endif--}}
				{{--<h1>{{ $course->title }}</h1>--}}
				{{--<br>--}}
				{{--<div class="description">--}}
					{{--<h3 class="title">Короткое описание</h3>--}}
					{{--<p>{{ $course->short_description }}</p>--}}
				{{--</div>--}}
				{{--<div class="description">--}}
					{{--<h3 class="title">Возраст</h3>--}}
					{{--<p>{{ $course->age->age }}</p>--}}
				{{--</div>--}}
				{{--@if($course->level)--}}
				{{--<div class="description">--}}
                    {{--<h3 class="title">Уровень</h3>--}}
                    {{--<p>{{ $course->level->level }}</p>--}}
                {{--</div>--}}
                {{--@endif--}}
				{{--@if($course->groups && !$course->groups->isEmpty())--}}
				{{--<div class="description">--}}
					{{--<h3 class="title">Среднее количество мест</h3>--}}
					{{--<p>{{ $course->averageSeatsCount() }}</p>--}}
				{{--</div>--}}
				{{--@endif--}}
				{{--<div class="school-info">--}}
					{{--<h3 class="title">{{ $course->school->title }}</h3>--}}
					{{--@if($course->school && $course->school->images)--}}
						{{--<div class="school-info-image">--}}
							{{--<img src="{{ $course->school->first_image }}" alt="{{ $course->school->title }}"/>--}}
						{{--</div>--}}
					{{--@endif--}}
					{{--<div class="school-info-text">--}}
						{{--<p>Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание Короткое описание</p>--}}
					{{--</div>--}}
					{{--<div class="clearfix"></div>--}}
				{{--</div>--}}
				{{--@if ($groups->count())--}}
				{{--<div class="description">--}}
					{{--<table class="course-table">--}}
						{{--<tr>--}}
							{{--<th>Время начала</th>--}}
							{{--<th>Кол-во уроков</th>--}}
                            {{--<th>Кол-во свободных мест</th>--}}
							{{--<th>Цена</th>--}}
							{{--<th></th>--}}
						{{--</tr>--}}
						{{--@foreach($groups as $group)--}}
						{{--<tr>--}}
							{{--<td>{{ $group->begins }}</td>--}}
							{{--<td>{{ $group->lessons }}</td>--}}
							{{--<td>{{ $group->seats_all - $group->seats_busy }}</td>--}}
							{{--<td>${{ number_format($group->price, 2, '.', ' ') }}</td>--}}
							{{--<td>--}}
								{{--<a class="btn btn-error" href="{{ URL::route('buy', [$group->id]) }}">Оплатить</a>--}}
							{{--</td>--}}
						{{--</tr>--}}
						{{--@endforeach--}}
					{{--</table>--}}
				{{--</div>--}}
				{{--@endif--}}
				{{--<div class="description">--}}
					{{--<h3 class="title">Рейтинг</h3>--}}
					{{--<p class="rating">--}}
						{{--<span>☆</span><span>☆</span><span>☆</span><span class="not">☆</span><span class="not">☆</span>--}}
					{{--</p>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="meta">--}}
			{{--<div class="place">--}}
				{{--<span class="icons icon-place"></span>{{ $course->metro }}--}}
			{{--</div>--}}
			{{--<div class="clearfix"></div>--}}
		{{--</div>--}}
		{{--<div class="clearfix"></div>--}}
	{{--</div>--}}
@stop
@section('scripts')
<script src="/js/jquery.datetimepicker.js" type="text/javascript"></script>
<script src="/js/add-group.constructor.js" type="text/javascript"></script>
<script src="/js/checkboxes.style.js" type="text/javascript"></script>
@stop