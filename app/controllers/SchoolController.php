<?php

use Laracasts\Commander\CommanderTrait;
use Site\Commands\School\GetCoursesListCommand;
use Site\Transformers\AgeTransformer;
use Site\Transformers\GroupTransformer;
use Site\Transformers\LevelTransformer;

class SchoolController extends \BaseController {

	use CommanderTrait;

	/**
	 * @var LevelTransformer
	 */
	private $levelTransformer;
	/**
	 * @var AgeTransformer
	 */
	private $ageTransformer;
	/**
	 * @var GroupTransformer
	 */
	private $groupTransformer;

	/**
	 * @param LevelTransformer $levelTransformer
	 * @param AgeTransformer   $ageTransformer
	 * @param GroupTransformer $groupTransformer
	 */
	public function __construct(
		LevelTransformer $levelTransformer,
		AgeTransformer $ageTransformer,
		GroupTransformer $groupTransformer)
	{
		$this->levelTransformer = $levelTransformer;
		$this->ageTransformer = $ageTransformer;
		$this->groupTransformer = $groupTransformer;
	}

	public function index($title)
	{
		$schoolTitle = trim(strip_tags($title));

		$ages      = $this->ageTransformer->transformCollection(Age::all(), true);
		$levels    = $this->levelTransformer->transformCollection(Level::all(), true);
		$metroList = MetroStation::all()->lists('name', 'id');
		foreach($metroList as $key => $name) {
			$metroList[$key] = preg_replace('/.* метро/', 'метро', $name);
		}

		$mainCats = Category::with([
			'childrens' => function ($q)
			{
				return $q->orderBy('sort');
			}
		])->mainCats()->sorted()->limit(7)->get();

		$limit = 4;
		$withDisabled = true;
		$result = $this->execute(GetCoursesListCommand::class, Input::all() + compact('schoolTitle', 'limit', 'withDisabled'));

		$courses = $result['courses'];
		$school = $result['school'];
		$command = $result['command'];
		$prices  = $result['prices'];
		$category  = $result['category'];
		$subCategories  = $result['subCategories'];
		$activeCategory  = $result['activeCategory'];

//		$title = trim(strip_tags($title));
//
//		if (!$title)
//			return Redirect::home();
//
//		$data = [];
//		$data['authorized'] = Auth::check();
//		$data['isAdmin'] = $this->isAdmin();
//		$data['school'] = School::whereSeoTitle($title)->active()->first();
//
//		if (!$data['school'])
//			return Redirect::route('catalog');
//
//		$data['mainCats'] = Category::query()->mainCats()->with(['childrens' => function($q) {return $q->orderBy('sort');}])->sorted()->get();
//		$data['ages'] = Age::all();
//		$data['levels'] = Level::all();
//		if (Auth::check()) {
//			$data['courses'] = $data['school']
//				->courses()
//				->with('groups')
//				->sorted()
//				->paginate(5);
//		}
//		else {
//			$data['courses'] = $data['school']
//				->courses()
//				->with('groups')
//				->active()
//				->sorted()
//				->paginate(5);
//		}
//
//		$data['prices'] = $data['courses']->lists('price');
//
		// Предоставлять ли возможность редактирования/добавления курсов/групп
		$isAdmin = $this->isAdmin();
		$editable = false;
		if (Auth::check()) {
			$user = Auth::user();

			if ($isAdmin  ) {
				$editable = true;
			}
			elseif ($school->user_id == $user->id) {
				$editable = true;
			}
		}

//		return View::make('school.catalog', $data);
		return View::make('school.catalog',
			compact('mainCats', 'ages', 'levels', 'prices', 'courses', 'metroList', 'subCategories',
				'category', 'command', 'activeCategory', 'school', 'editable', 'isAdmin'));
	}

	public function getAddCourse()
	{
		$data = [];
		$data['ages'] = Age::all();
		$data['levels'] = Level::all();
		$data['cats'] = Category::query()->mainCats()->with('childrens')->get();
		if (Auth::user()->school) {
			$data['school'] = Auth::user()->school;
		}
		else {
			$data['school'] = null;
			$data['allSchools'] = School::all();
		}

		return View::make('school.add_course', $data);
	}

	public function postAddCourse()
	{
		if (Auth::user()->school) {
			$school_id = Auth::user()->school->id;
		}
		else {
			$school_id = trim(strip_tags(Input::get('school')));
		}

		$validation = Validator::make(Input::all(), Course::$validationRules);

		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput(Input::all());
		}

		$course = new Course();
		$course->title = Input::get('title');
		$course->seo_title = $course->generateSlug();
		$course->lessons = Input::get('lessons');
		$course->duration = preg_replace('/\,/', '/\./', Input::get('duration'));
		$course->short_description = Input::get('short_description');
		$course->full_description = Input::get('full_description');
		$course->location_addr = Input::get('location_address');
		$course->metro_id = MetroStation::getMetroId(Input::get('location_metro'));
		$course->level_id = Input::get('level_id');
		$course->age_id = Input::get('age_id');
		$course->category_id = Input::get('category_id');
		$course->status = 1;
		$course->school_id = $school_id;
		$course->images = Input::get('images');

		$course->save();

		return Redirect::route('school', [$course->school->seo_title])->with('message', 'Курс успешно добавлен');
	}

	public function getEditCourse($title)
	{
		$course = Data::get('course');
		if (!$course) {
			return Redirect::route('school', [Auth::user()->school->seo_title])->with('message', 'Указанный курс не найден');
		}

		$data = [
			'cats' => Category::query()->with('childrens')->get(),
			'course' => $course,
			'ages' => Age::all(),
			'levels' => Level::all()
		];

		return View::make('courses.edit_course', $data);
	}

	public function postEditCourse($title)
	{
		$course = Data::get('course');
		if (!$course) {
			return Redirect::route('catalog')->with('message', 'Указанный курс не найден');
		}

		$rules = Course::$validationRules;
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->fails()) {
			return Redirect::route('course-edit', [$course->seo_title])->withInput(Input::all())->withErrors($validation);
		}

		$course->setTitle(Input::get('title'));
		$course->short_description = Input::get('short_description');
		$course->full_description = Input::get('full_description');
		$course->images = Input::get('images');
		$course->location_addr = Input::get('location_address');
		$course->metro_id = MetroStation::getMetroId(Input::get('location_metro'));
		$course->category_id = Input::get('category_id');
		$course->age_id = Input::get('age_id');
		$course->level_id = Input::get('level_id');
		$course->save();

		return Redirect::route('course-edit', [$course->seo_title])->with('message', 'Курс успешно сохранен!');
	}
}