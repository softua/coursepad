<?php

class BaseController extends Controller
{

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function isAdmin()
	{
		$isAdmin = false;

		if (Auth::guest())
			return $isAdmin;

		$user = Auth::user();
		foreach($user->roles()->get() as $role) {
			if ($role->id == 1 || $role->id == 2)
				$isAdmin = true; break;
		}

		return $isAdmin;
	}
}
