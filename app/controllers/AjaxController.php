<?php

class AjaxController extends BaseController
{
	public function getImage()
	{
		if (!Request::ajax()) {
			return Redirect::home();
		}

		if (Input::hasFile('images')) {
			$newName = time() . rand(1, 1000) . '.' . Input::file('images')->getClientOriginalExtension();

			$path = public_path() . '/uploads/school';
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}

			$file = Input::file('images');
			$file->move($path, $newName);

			return Response::json(['id' => $newName]);
		}
		else {
			return Response::json(false);
		}
	}

	public function getImageCourse()
	{
		if (!Request::ajax()) {
			return Redirect::home();
		}

		if (Input::hasFile('images')) {
			$newName = time() . rand(1, 1000) . '.' . Input::file('images')->getClientOriginalExtension();

			$path = public_path() . '/uploads/course';
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}

			$file = Input::file('images');
			$file->move($path, $newName);

			return Response::json(['id' => $newName]);
		}
		else {
			return Response::json(false);
		}
	}

	public function addGroup($courseId)
	{
		if (!Request::ajax()) {
			return Redirect::home();
		}

		$course = Course::find($courseId);
		if (!$course) {
			return Response::json([
				'result' => false,
				'errors' => ['Нет такого курса!']
			]);
		}
		else {
			// Массив типа дата время;кол-во уроков;кол-во мест;стоимость;
			$errors = [];

			// Время не может быть меньше текущего
			$tempTime = new DateTime(Input::get('date_start'));
			if ($tempTime <= new DateTime()) {
				$errors[] = 'Неверно указано время';
			}

			// Кол-во уроков не может быть меньше 1
			if (Input::get('lessons_count') < 1) {
				$errors[] = 'Неверно указано кол-во уроков';
			}

			if (count($errors)) {
				return Response::json([
					'result' => false,
					'errors' => $errors
				]);
			}
			else {
				$group = new Group();
				$group->begins = $tempTime->format('Y-m-d H:i');
				$group->lessons = (int)Input::get('lessons_count');
				$group->seats_all = (int)Input::get('place_count');
				$group->price = (float)Input::get('price');
				$group->course_id = $course->id;
			}

			if ($group->save()) {
				return Response::json([
					'result' => true,
					'id' => $group->id,
					'url_edit' => URL::action('AjaxController@editGroup', [$group->id]),
					'url_delete' => URL::action('AjaxController@deleteGroup', [$group->id])
				]);
			}
			else {
				return Response::json([
					'result' => false,
					'errors' => ['Ошибка в БД']
				]);
			}
		}
	}

	public function editGroup($groupId)
	{
		if (!Request::ajax()) {
			return Redirect::home();
		}

		$group = Group::find($groupId);
		if (!$group) {
			return Response::json([
				'result' => false,
				'errors' => ['Нет такой группы']
			]);
		}
		else {
			$errors = [];

			$tempTime = new DateTime(Input::get('date_start'));
			// Дата не может быть меньше текущей
			if ($tempTime <= new DateTime()) {
				$errors[] = 'Указано неверное время';
			}
			else {
				$group->begins = $tempTime;
			}

			// Кол-во уроков не может быть меньше 1
			if (Input::get('lessons_count') < 1) {
				$errors[] = 'Кол-во уроков должно быть не менее 1';
			}
			else {
				$group->lessons = (int)Input::get('lessons_count');
			}

			// Количество мест не может быть меньше занятых
			if(Input::get('place_count') < $group->seats_busy) {
				$errors[] = 'Неверно указано кол-во мест';
			}
			else {
				$group->seats_all = (int)Input::get('place_count');
			}

			$group->price = floatval(Input::get('price'));

			if (count($errors)) {
				return Response::json([
					'result' => false,
					'errors' => $errors
				]);
			}
			else {
				if($group->save()) {
					return Response::json([
						'result' => true
					]);
				}
				else {
					return Response::json([
						'result' => false,
						'errors' => ['Ошибка в БД']
					]);
				}
			}
		}
	}

	public function deleteGroup($groupId)
	{
		if (!Request::ajax()) {
			return Redirect::home();
		}
		else {
			$groupId = (int)trim(strip_tags($groupId));
			$group = Group::find($groupId);
			if (!$group) {
				return Response::json([
					'result' => false,
					'errors' => ['Нет такой группы']
				]);
			}

			if (count($group->seats_busy)) {
				return Response::json([
					'result' => false,
					'errors' => ['Невозможно удалить группу']
				]);
			}
			else {
				if($group->delete()) {
					return Response::json([
						'result' => false,
						'errors' => ['Не получилось удалить группу']
					]);
				}
				else {
					return Response::json([
						'result' => true
					]);
				}
			}
		}
	}
}