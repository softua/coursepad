<?php
use Laracasts\Commander\CommanderTrait;
use Site\Commands\Course\BuyCourseCommand;
use Site\Commands\Course\GetCoursesListCommand;
use Site\Transformers\AgeTransformer;
use Site\Transformers\GroupTransformer;
use Site\Transformers\LevelTransformer;

/**
 * Created by Ruslan Koloskov
 * Date: 31.08.2014
 * Time: 13:05
 */
class CoursesController extends BaseController {

    use CommanderTrait;
    /**
     * @var LevelTransformer
     */
    private $levelTransformer;
    /**
     * @var AgeTransformer
     */
    private $ageTransformer;
    /**
     * @var GroupTransformer
     */
    private $groupTransformer;

    /**
     * @param LevelTransformer $levelTransformer
     * @param AgeTransformer   $ageTransformer
     * @param GroupTransformer $groupTransformer
     */
    public function __construct(
        LevelTransformer $levelTransformer,
        AgeTransformer $ageTransformer,
        GroupTransformer $groupTransformer)
    {
        $this->levelTransformer = $levelTransformer;
        $this->ageTransformer   = $ageTransformer;
        $this->groupTransformer = $groupTransformer;
    }

    /**
     * @param null $categoryTitle
     *
     * @return \Illuminate\View\View
     */
    public function index($categoryTitle = null, $subCategoryTitle = null)
    {
        $categoryTitle = trim(strip_tags($categoryTitle));
        $subCategoryTitle = trim(strip_tags($subCategoryTitle));

        $ages      = $this->ageTransformer->transformCollection(Age::all(), true);
        $levels    = $this->levelTransformer->transformCollection(Level::all(), true);
        $metroList = [
            1 => 'Арбатская',
            2 => 'Киевская',
            3 => 'Третьяковская',
        ];

        $mainCats = Category::with([
            'childrens' => function ($q)
            {
                return $q->orderBy('sort');
            }
        ])->mainCats()->sorted()->limit(7)->get();


        $result = $this->execute(GetCoursesListCommand::class, Input::all() + compact('categoryTitle', 'subCategoryTitle'));

        $courses = $result['courses'];
        $command = $result['command'];
        $prices  = $result['prices'];
        $category  = $result['category'];
        $subCategories  = $result['subCategories'];
        $activeCategory  = $result['activeCategory'];

        return View::make('courses.catalog',
            compact('mainCats', 'ages', 'levels', 'prices', 'courses', 'metroList', 'subCategories',
                'category', 'command', 'activeCategory'));
    }

    /**
     * @param $title
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($title)
    {
        $course = Course::whereSeoTitle($title)->with([
            'school',
            'groups',
            'category',
            'category.parent',
        ])->first();
        if(!$course)
        {
            return Redirect::route('catalog');
        }

        $groups = $course->groups()->active()->visible()->sorted()->get();

        $closestGroup  = $groups->shift();
        $subCategories = null;

        $commonCourses = Course::with([
            'school',
            'groups',
            'category',
            'category.parent',
        ])->where('category_id', $course->category->id)->take(3)->get();

        return View::make('courses.course_description',
            compact('course', 'groups', 'subCategories', 'commonCourses', 'closestGroup'));
    }

    /**
     * @param $groupId
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getBuy($groupId)
    {
        $group = Group::with('course', 'course.groups')->find(intval($groupId));

        if(!$group)
        {
            if(Request::header('referrer'))
            {
                return Redirect::back();
            }
            else
            {
                return Redirect::home();
            }
        }

        $course       = $group->course;
        $groups       = $course->groups()->active()->visible()->sorted()->get();
        $groupsSelect = $this->groupTransformer->transformCollection($groups, true);
        $groupsData   = $this->groupTransformer->transformCollection($groups);

        $subCategories = null;

        return View::make('courses.buy', compact('group', 'course', 'groupsSelect', 'groupsData', 'subCategories'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postBuy()
    {
        /**
         * @var Order $order
         */
        $order = $this->execute(BuyCourseCommand::class);

        $url            = Config::get('robokassa.url');
        $MerchantLogin  = Config::get('robokassa.login');
        $OutSum         = number_format($order->amount, 2, '.', '');
        $InvoiceID      = $order->id;
        $Description    = 'Оплата курса ' . $order->group->course->title;
        $SignatureValue = md5(Config::get('robokassa.login') . ':' . number_format($order->amount, 2, '.', '') . ':' . $order->id . ':'
            . Config::get('robokassa.pass1'));
        $Language       = 'ru';

        $query_data = compact('MerchantLogin', 'OutSum', 'InvoiceID', 'SignatureValue', 'Language', 'Description');

        return Redirect::away($url . '?' . http_build_query($query_data));
    }
}