<?php
/**
 * Created by Ruslan Koloskov
 * Date: 11.09.2014
 * Time: 21:40
 */

class UserController extends BaseController
{
	public function register()
	{
		return View::make('school.registration');
	}

	public function postRegister()
	{
		$rules = User::$rules;
		// Добавляем уникальность названия школы
		$rules['title'] = 'alpha_dash_space|unique:schools,title|min:2';

		Validator::extend('alpha_dash_space', function($attribute, $value, $params) {
			$pattern = '/^[а-яА-ЯёЁa-zA-Z0-9 ]+$/u';
			$result = preg_match($pattern, trim(strip_tags($value)));
			return $result;
		});

		$validation = Validator::make(Input::all(), $rules);

		if ($validation->fails()) {
			return Redirect::route('register')->withErrors($validation)->withInput();
		}

		$user = new User(Input::all());
		if ($user->register()) {
			$school = new School(Input::all());
			$school->seo_title = Translit::get_seo_keyword($school->title, true);
			$school->save();
			$user->school()->save($school);

			if (Input::has('image_id') && strlen(Input::get('image_id'))) {
				$school->img_url = trim(strip_tags(Input::get('image_id')));
				$school->save();
			}

			$user->sendActivationEmail();

			return View::make('pages.message', [
				'messageTitle' => 'Необходима активация',
				'messageBody' => 'Спасибо за регистрацию. На Ваш email отправлено письмо с ссылкой для активации аккаунта.'
			]);
		}
		else {
			return Redirect::back()->withInput()->witAlert();
		}
	}

	public function getActivate($userId, $activationCode)
	{
		// @var User $user
		$user = User::find(trim(strip_tags($userId)));
		if (!$user) {
			return Redirect::home();
		}

		$code = trim(strip_tags($activationCode));

		if (!$code || $code != $user->activation_code) {
			return View::make('pages.message', [
				'messageTitle' => 'Ошибка активации',
				'messageBody' => 'Активация завершилась неудачей. К сожалению Ваш аккаунт не активирован'
			]);
		}
		else {
			$user->activation_code = null;
			$user->confirmed = 1;
			$user->save();

			$school = $user->school;

			if ($school) {
				$school->status = 1;
				$school->save();
			}

			// залогиниваем юзера
			Auth::login($user, true);

			return Redirect::home();
		}
	}

	public function login()
	{
		if (Request::isMethod('GET'))
		{
			if (!Request::ajax()) {
				if (!Auth::check()) {
					return View::make('user.login');
				}
				else {
					return Redirect::intended();
				}
			}
			else {
				return Response::json(false);
			}
		}
		else {
			Log::debug(Input::all());
			$data = [];
			$data['email'] = trim(strip_tags(Input::get('email')));
			$data['password'] = trim(strip_tags(Input::get('password')));

			$logedIn = Auth::attempt($data, true);
			// Подтверждена ли почта
			if ($logedIn) {
				$logedIn = Auth::user()->confirmed;
			}

			if ($logedIn && Request::ajax()) {
				$response = [
					'result' => true,
					'user' => Auth::user()->contact_name
				];
				if (Auth::user()->school) {
					$path = '/uploads/' . Auth::user()->school->id;
					if (!file_exists(public_path() . $path)) {
						mkdir(public_path() . $path);
					}
					//TODO записываем путь в файл
					return Response::json($response);
				}
				else {
					return Response::json($response);
				}

			}
			elseif ($logedIn && !Request::ajax()) {
				if (Auth::user()->school) {
					$path = '/uploads/' . Auth::user()->school->id;
					if (!file_exists(public_path() . $path)) {
						mkdir(public_path() . $path);
					}
					//TODO записываем путь в файл
					return Redirect::intended();
				}
				else {
					return Redirect::intended();
				}
			}
			elseif (!$logedIn && Request::ajax()) {
				$response = [
					'result' => false,
					'user' => null
				];
				return Response::json($response);
			}
			elseif (!$logedIn && !Request::ajax()) {
				return View::make('user.login', [
					'message' => 'Что-то пошло не так!'
				]);
			}
		}
	}

	public function logout()
	{
		Auth::logout();
		if (Request::get('referer')) {
			return Redirect::back();
		}
		else {
			return Redirect::home();
		}

	}
} 