<?php
/**
 * Created by Ruslan Koloskov
 * Date: 29.09.2014
 * Time: 21:56
 */

class PaymentController extends BaseController
{
	public function result()
	{
		Debugbar::disable();
		$order = Order::whereId(Input::get('InvId'))->wherePaid(0)->first();
		if (!$order) {
			return 'false';
		}

		if ($order->amount != Input::get('OutSum')) {
			return 'false';
		}

		$oldCrc = strtoupper(Input::get('SignatureValue'));
		$newCrc = strtoupper(md5(number_format($order->amount, 2, '.', '') . ':' . $order->id . ':' . Config::get('robokassa.pass2')));

		if ($oldCrc != $newCrc) {
			return 'false';
		}

		return 'OK'.$order->id;
	}

	public function success()
	{
		$order = Order::whereId(Input::get('InvId'))->wherePaid(0)->first();
		if (!$order) {
			return View::make('pages.message', [
				'messageTitle' => 'Ошибка оплаты',
				'messageBody' => 'К сожалению, с оплатой что-то не так'
			]);
		}

		if (number_format($order->amount, 2, '.', '') != Input::get('OutSum')) {
			return View::make('pages.message', [
				'messageTitle' => 'Ошибка оплаты',
				'messageBody' => 'К сожалению, оплаченная сумма не совпадает с запрошенной'
			]);
		}

		$oldCrc = strtoupper(Input::get('SignatureValue'));
		$newCrc = strtoupper(md5(number_format($order->amount, 2, '.', '') . ':' . $order->id . ':' . Config::get('robokassa.pass1')));

		if ($oldCrc != $newCrc) {
			return View::make('pages.message', [
				'messageTitle' => 'Ошибка оплаты',
				'messageBody' => 'К сожалению, что-то пошло не так'
			]);
		}
		else {
			$order->paid = 1;
			$order->save();

			$payer = $order->clients()->wherePayer(1)->first();
			$listenersNames = [];
			foreach($order->clients()->whereListener(1)->get() as $listener) {
				$listenersNames[] = $listener->first_name . ' ' . $listener->last_name;
			}

			Mail::send('emails.payment.success', ['order' => $order, 'payer' => $payer, 'listeners' => $listenersNames], function($message) use ($payer) {
				$message->from(getenv('MAIL_FROM.address'), getenv('MAIL_FROM.name'));
				$message->to($payer->email);
				$message->subject('Добро пожаловать на обучение в coursepad.ru');
			});

			return View::make('pages.message', [
				'messageTitle' => 'Оплата прошла успешно',
				'messageBody' => 'Уважаемый ' . $payer->first_name . ', Вы записались на курс "' . $order->group->course->title . '". <br> Желаем удачного обучения.'
			]);
		}
	}

	public function fail()
	{
		$order = Order::query()->whereId(Input::get('InvId'))->first();
		if ($order) {
			$order->paid = 2;
			$order->save();

			return View::make('pages.message', [
				'messageTitle' => 'Оплата отменена',
				'messageBody' => 'К сожалению, оплата отменена. <br> Вы не сможете пройти курс ' . $order->group->course->title
			]);
		}
		else {
			return Redirect::home();
		}
	}
} 