<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CoursesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$categories = Category::whereNotNull('parent_id')->lists('id');
		$schools = School::lists('id');

		foreach(range(1, 50) as $index)
		{
			$title = $faker->sentence($nbWords = rand(1, 5));
			$seoTitle = Translit::get_seo_keyword($title, true);

			Course::create([
				'title' => $title,
				'seo_title' => $seoTitle,
				'short_description' => $faker->paragraph($nbSentences = rand(2, 5)),
				'full_description' => $faker->paragraph($nbSentences = rand(5, 8)),
				'location_addr' => $faker->streetName,
				'level_id' => rand(1, 4),
				'age_id' => rand(1, 4),
				'category_id' => $faker->randomElement($categories),
				'status' => rand(0, 1),
				'school_id' => $faker->randomElement($schools),
				'lessons' => $faker->numberBetween(1, 20),
				'duration' => $faker->randomFloat(1, 0, 3),
			]);
		}
	}
}