<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('AgesTableSeeder');
		$this->call('LevelsTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('CoursesTableSeeder');
		$this->call('ReviewsTableSeeder');
		$this->call('GroupsTableSeeder');
//		$this->call('ClientsTableSeeder');
	}
}