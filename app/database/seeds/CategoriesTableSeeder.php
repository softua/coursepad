<?php

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$titles = [
			'Полезное', 'Искусство', 'Творчество', 'Еда', 'Танцы', 'Спорт',
			'Языки', 'Музыка', 'Бизнес', 'Технологии', 'Профессии', 'Детям',
		];

		foreach($titles as $main) {
			Category::create([
				'title' => "$main",
				'seo_title' => Translit::get_seo_keyword($main, true),
			]);
		}

		$mainCats = Category::all();

		foreach($mainCats as $mainCat)
		{
			foreach(range(1, rand(2, 30)) as $number)
			{
				$title = 'Подкатегория ' . $number;
				$seoTitle = Translit::get_seo_keyword($title, true);
				Category::create([
					'title' => $title,
					'seo_title' => $seoTitle,
					'parent_id' => $mainCat->id,
				]);
			}
		}
	}
}