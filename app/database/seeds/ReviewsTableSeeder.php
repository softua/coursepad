<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ReviewsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 100) as $index)
		{
			Review::create([
				'username' => $faker->name,
				'rating' => rand(1, 5),
				'description' => $faker->sentence($nbWords = rand(1, 15)),
				'course_id' => rand(1, 50)
			]);
		}
	}
}