<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LevelsTableSeeder extends Seeder {

	public function run()
	{
		Level::create([
			'level' => 'открытый'
		]);

		Level::create([
			'level' => 'начинающий'
		]);

		Level::create([
			'level' => 'средний'
		]);

		Level::create([
			'level' => 'профессионал'
		]);
	}
}