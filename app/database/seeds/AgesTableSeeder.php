<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AgesTableSeeder extends Seeder {

	public function run()
	{
		Age::create([
			'age' => 'любой'
		]);

		Age::create([
			'age' => 'дети'
		]);

		Age::create([
			'age' => 'подростки'
		]);

		Age::create([
			'age' => 'взрослые'
		]);
	}
}