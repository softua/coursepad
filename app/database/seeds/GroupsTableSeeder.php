<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class GroupsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$coursesIds = Course::lists('id');

		foreach($coursesIds as $courseId)
		{
			$groups = rand(4, 15);
			for($i=0; $i<=$groups; $i++)
			{
				Group::create([
					'course_id' => $courseId,
					'begins' => $faker->dateTimeBetween($startDate = '+1 hour', $endDate = '+1 year'),
					'lessons' => rand(1, 30),
					'seats_all' => rand(5, 30),
					'price' => $faker->randomFloat($nbMaxDecimals = 0, $min = 30, $max = 15000),
					'visibility' => rand(0, 1),
					'status' => rand(0, 1)
				]);
			}
		}
	}

}