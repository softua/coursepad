<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		// Админы
		$superAdmin = User::create([
			'email' => 'softua6@gmail.com',
			'password' => Hash::make('qwerty12345'),
			'contact_name' => 'Soft UA',
			'contact_phone' => '0949307076',
			'confirmed' => 1
		]);
		$superAdmin->roles()->attach(1);

		$admin = User::create([
			'email' => 'test@test.com',
			'password' => Hash::make('qwerty12345'),
			'contact_name' => 'Админ',
			'confirmed' => 1
		]);
		$admin->roles()->attach(2);

		// Школы
		foreach(range(1, 30) as $index)
		{
			$user = User::create([
				'email' => $faker->unique()->email(),
				'password' => Hash::make('qwerty12345'),
				'contact_name' => $faker->name,
				'contact_phone' => $faker->optional()->phoneNumber,
				'activation_code' => null,
				'confirmed' => 1
			]);
			$user->roles()->attach(3);

			$title = 'Школа "' . $faker->unique()->sentence($nbWords = 2) . '"';
			$seoTitle = Translit::get_seo_keyword($title, true);

			$school = School::create([
				'title' => $title,
				'seo_title' => $seoTitle,
				'short_description' => $faker->paragraph($nbSentencs = 3),
				'img_url' => $faker->imageUrl(640, 480),
				'status' => rand(0, 1)
			]);

			$user->school()->save($school);


		}
	}

}