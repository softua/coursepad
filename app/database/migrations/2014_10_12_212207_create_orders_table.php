<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->float('amount');
			$table->unsignedInteger('group_id')->nullable();
			$table->foreign('group_id')->references('id')->on('groups');
			$table->integer('paid');
			$table->datetime('booking');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table) {
			$table->dropForeign('orders_group_id_foreign');
		});

		Schema::drop('orders');
	}

}
