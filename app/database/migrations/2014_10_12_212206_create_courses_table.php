<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255);
			$table->string('seo_title', 255);
			$table->text('short_description', 65535)->nullable();
			$table->text('full_description', 65535)->nullable();
			$table->text('images', 65535)->nullable();
			$table->string('location_addr', 255)->nullable();
			$table->unsignedInteger('metro_id')->nullable();
			$table->unsignedInteger('level_id');
			$table->foreign('level_id')->references('id')->on('levels');
			$table->unsignedInteger('age_id');
			$table->foreign('age_id')->references('id')->on('ages');
			$table->unsignedInteger('category_id')->nullable();
			$table->foreign('category_id')->references('id')->on('categories');
			$table->integer('status');
			$table->unsignedInteger('school_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('courses', function(Blueprint $table) {
			$table->dropForeign('courses_level_id_foreign');
			$table->dropForeign('courses_age_id_foreign');
			$table->dropForeign('courses_category_id_foreign');
		});

		Schema::drop('courses');
	}

}
