<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->string('username', 255)->nullable();
			$table->integer('rating');
			$table->text('description', 65535)->nullable();
			$table->unsignedInteger('course_id');
			$table->foreign('course_id')->references('id')->on('courses');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function(Blueprint $table) {
			$table->dropForeign('reviews_course_id_foreign');
		});

		Schema::drop('reviews');
	}

}
