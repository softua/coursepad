<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLastNameToClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clients', function(Blueprint $table)
		{
			$table->renameColumn('name', 'last_name');
			$table->string('first_name', 255)->after('id');
			$table->string('phone', 15)->nullable()->after('email');
			DB::statement('ALTER TABLE `clients` MODIFY `order_id` INTEGER (10) UNSIGNED NULL;');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clients', function(Blueprint $table)
		{
			$table->renameColumn('last_name', 'name');
			$table->dropColumn('first_name');
			$table->dropColumn('phone');
			DB::statement('ALTER TABLE `clients` MODIFY `order_id` INTEGER (10) UNSIGNED;');
		});
	}

}
