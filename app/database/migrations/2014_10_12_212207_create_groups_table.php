<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('course_id');
			$table->foreign('course_id')->references('id')->on('courses');
			$table->datetime('begins');
			$table->integer('lessons');
			$table->integer('seats_all')->nullable();
			$table->integer('seats_busy');
			$table->float('price');
			$table->integer('visibility');
			$table->integer('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('groups', function(Blueprint $table) {
			$table->dropForeign('groups_course_id_foreign');
		});

		Schema::drop('groups');
	}

}
