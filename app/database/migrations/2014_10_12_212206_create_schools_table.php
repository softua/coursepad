<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schools', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255);
			$table->string('seo_title', 255)->unique();
			$table->text('short_description', 65535)->nullable();
			$table->text('img_url', 65535)->nullable();
			$table->integer('status');
			$table->unsignedInteger('user_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('schools', function(Blueprint $table) {
			$table->dropForeign('schools_user_id_foreign');
		});

		Schema::drop('schools');
	}

}
