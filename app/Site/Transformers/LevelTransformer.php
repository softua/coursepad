<?php namespace Site\Transformers;

class LevelTransformer extends Transformer{

    /**
     * Преобразование из массива
     *
     * @param array $item
     *
     * @return mixed
     */
    public function transform($item)
    {
        // TODO: Implement transform() method.
    }

    /**
     * Преобразование из объекта
     *
     * @param $item
     *
     * @return mixed
     */
    public function transformObject($level)
    {
        return [
            'id' => $level->id,
            'name' => $level->level,
        ];
    }

    /**
     * Преобразование для select поля
     *
     * @param array $item
     *
     * @return mixed
     */
    protected function transformForSelect($levels)
    {
        $levelsForSelect = [];
        foreach($levels as $level)
        {
            $levelsForSelect[$level['id']] = $level['name'];
        }

        return $levelsForSelect;
    }
}