<?php namespace Site\Transformers;

use Group;

class GroupTransformer extends Transformer {

    /**
     * Преобразование из массива
     *
     * @param array $item
     *
     * @return mixed
     */
    public function transform($item)
    {
        // TODO: Implement transform() method.
    }

    /**
     * Преобразование из объекта
     *
     * @param Group $group
     *
     * @return mixed
     */
    public function transformObject($group)
    {
        return [
            'id' => $group->id,
            'begins' => $group->present()->courseBegins,
            'course' => $group->course_id,
            'lessons' => $group->lessons,
            'seats_all' => $group->seats_all,
            'seats_busy' => $group->seats_busy,
            'seats_left' => $group->getSeatsLeft(),
            'price' => $group->price,
        ];
    }

    /**
     * Преобразование для select поля
     *
     * @param array $item
     *
     * @return mixed
     */
    protected function transformForSelect($groups)
    {
        if(!$groups)
            return [];

        $groupsForSelect = [];
        foreach($groups as $group)
        {
            $groupsForSelect[$group['id']] = $group['begins'];
        }

        return $groupsForSelect;
}}