<?php namespace Site\Transformers;

class AgeTransformer extends Transformer {

    /**
     * Преобразование из массива
     *
     * @param array $item
     *
     * @return mixed
     */
    public function transform($item)
    {
        // TODO: Implement transform() method.
    }

    /**
     * Преобразование из объекта
     *
     * @param $item
     *
     * @return mixed
     */
    public function transformObject($age)
    {
        return [
            'id' => $age->id,
            'name' => $age->age,
        ];
    }

    /**
     * Преобразование для select поля
     *
     * @param array $ages
     *
     * @return mixed
     */
    protected function transformForSelect($ages)
    {
        $agesForSelect = [];
        foreach($ages as $age)
        {
            $agesForSelect[$age['id']] = $age['name'];
        }

        return $agesForSelect;
}}