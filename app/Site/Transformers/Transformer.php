<?php namespace Site\Transformers;

/**
 * Class Transformer
 *
 * @package Site\Transformers
 */
abstract class Transformer {

    /**
     * @param $items
     *
     * @return array
     */
    public function transformCollection($items, $transformForSelect = false)
    {
        if(is_object($items))
        {
            $transformed_data = null;
            foreach($items as $item)
            {
                $transformed_item = $this->transformObject($item);

                if($transformed_item)
                {
                    $transformed_data[] = $transformed_item;
                }
            }
        }
        else
        {
            $transformed_data = array_map([$this, 'transform'], $items);
        }

        if( ! $transformForSelect)
        {
            return $transformed_data;
        }

        return $this->transformForSelect($transformed_data);
    }

    /**
     * Преобразование из массива
     *
     * @param array $item
     *
     * @return mixed
     */
    public abstract function transform($item);

    /**
     * Преобразование из объекта
     * @param $item
     *
     * @return mixed
     */
    public abstract function transformObject($item);

    /**
     * Преобразование для select поля
     * @param array $item
     *
     * @return mixed
     */
    protected abstract function transformForSelect($items);
}