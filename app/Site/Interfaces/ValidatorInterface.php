<?php namespace Site\Interfaces;

/**
 * Interface Validator
 *
 * @package Site\Interfaces
 */
interface ValidatorInterface {

    /**
     * @param Object $command
     *
     * @return mixed
     */
    public function validate($command);
}