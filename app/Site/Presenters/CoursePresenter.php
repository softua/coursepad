<?php namespace Site\Presenters;

use Course;
use Lang;
use Laracasts\Presenter\Presenter;

class CoursePresenter extends Presenter{

    /**
     * @var Course
     */
    protected $entity;

    /**
     * Количество занятий в курсе
     *
     * @return string
     */
    public function lessons()
    {
        return $this->entity->lessons .' '. Lang::choice('занятие|занятия|занятий', $this->entity->lessons);
    }

    /**
     * Длительность урока в часах
     *
     * @return string
     */
    public function duration()
    {
        return $this->entity->duration. ' '. Lang::choice('час|часа|часов', $this->entity->duration);
    }
}