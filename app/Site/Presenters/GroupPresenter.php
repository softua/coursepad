<?php namespace Site\Presenters;

use App;
use Group;
use Lang;
use Laracasts\Presenter\Presenter;

class GroupPresenter extends Presenter {

    /**
     * @return mixed
     */
    public function courseBegins()
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');
        /**
         * @var Group $this
         */
        return $this->entity->begins->formatLocalized('%d %b %Y c %R');
    }

    /**
     * Оставшиеся места
     *
     * @return string
     */
    public function seatsLeft()
    {
        return  Lang::choice('место|места|мест', $this->entity->getSeatsLeft());
    }

    public function orderAmount($listenersCount)
    {

    }
}