<?php namespace Site\Flash;

use Illuminate\Support\MessageBag;
use Laracasts\Flash\FlashNotifier as LaracastsFlashNotifier;
use Laracasts\Flash\SessionStore;

class FlashNotifier extends LaracastsFlashNotifier {

    /**
     * @var SessionStore
     */
    private $session;

    /**
     * @param SessionStore $session
     */
    function __construct(SessionStore $session)
    {
        $this->session = $session;
    }

    /**
     * @param $message
     * @param $title
     */
    public function overlay($message, $title = 'Notice')
    {
        $this->message($message, 'info', $title);
        $this->session->flash('flash_notification.overlay', true);
        $this->session->flash('flash_notification.title', $title);
    }

    /**
     * @param $message
     * @param string $level
     */
    public function message($message, $level = 'info', $title = 'Notice')
    {
        $error_sections = [];
        if($message instanceof MessageBag)
        {
            $error_sections = array_keys($message->getMessages());
            $message = array_flatten($message->getMessages());
        }

        $this->session->flash('flash_notification.message', $message);
        $this->session->flash('flash_notification.sections', $error_sections);
        $this->session->flash('flash_notification.level', $level);
    }

}