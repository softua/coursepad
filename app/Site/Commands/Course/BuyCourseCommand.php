<?php namespace Site\Commands\Course;

class BuyCourseCommand {

    /**
     * @var int
     */
    public $group_id;
    /**
     * @var string
     */
    public $first_name;
    /**
     * @var string
     */
    public $last_name;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $email;
    /**
     * @var null|int
     */
    public $me_will_go;
    /**
     * @var array
     */
    public $listeners;

    /**
     * @param       $group_id
     * @param       $first_name
     * @param       $last_name
     * @param       $phone
     * @param       $email
     * @param null  $me_will_go
     * @param array $listeners
     */
    public function __construct(
        $group,
        $first_name,
        $last_name,
        $phone,
        $email,
        $me_will_go = null,
        $listeners = [])
    {

        $this->group_id   = $group;
        $this->first_name = $first_name;
        $this->last_name  = $last_name;
        $this->phone      = $phone;
        $this->email      = $email;
        $this->me_will_go = $me_will_go;
        $this->listeners  = $listeners;
    }
}