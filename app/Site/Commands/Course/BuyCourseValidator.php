<?php namespace Site\Commands\Course;

use Group;
use Site\Exceptions\ValidationException;
use Site\Interfaces\ValidatorInterface;
use Validator;

class BuyCourseValidator implements ValidatorInterface{

    /**
     * @param BuyCourseCommand $command
     *
     * @return mixed
     */
    public function validate($command)
    {
        Validator::extend('alpha_ru', function($attribute, $value, $params) {
            $pattern = '/^[а-яА-ЯёЁa-zA-Z\s]+$/u';
            $result = preg_match($pattern, trim(strip_tags($value)));
            return $result;
        }, 'Поле :attribute должно содержать только буквы и пробелы');

        // Подсчёт свободных мест
        $listenersCount = count($command->listeners);
        if($command->me_will_go)
            $listenersCount++;

        $group = Group::find($command->group_id);

        $labels = [
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'email' => 'Контактный E-mail',
            'phone' => 'Контактный телефон',
            'group_id' => 'Группа',
            'listeners' => 'Слушатели',
            'listenersCount' => 'Количество слушателей'
        ];

        $validator = Validator::make(
            [
                'last_name' => $command->last_name,
                'first_name' => $command->first_name,
                'email' => $command->email,
                'phone' => $command->phone,
                'me_will_go' => $command->me_will_go,
                'group_id' => $command->me_will_go,
                'listeners' => $command->listeners,
                'listenersCount' => $listenersCount
            ],
            [
                'group_id'  => 'exists:groups,id',
                'last_name' => 'required|alpha_ru',
                'first_name' => 'required|alpha_ru',
                'email' => 'required|email',
                'listeners' => 'array',
                'listenersCount' => 'min:1|max:'.$group->getSeatsLeft()
            ],
            [
                'max' => 'Количество слушателей превышает оставшиеся в группе места ('.$group->getSeatsLeft().').'
            ],
            $labels
        );

        if($validator->fails())
            throw new ValidationException($validator);

        foreach($command->listeners as $listener)
        {
            $validator = Validator::make(
                [
                    'last_name' => array_get($listener, 'last_name'),
                    'first_name' => array_get($listener, 'first_name'),
                ],
                [
                    'last_name' => 'required|alpha_ru',
                    'first_name' => 'required|alpha_ru',
                ],
                [],
                $labels
            );

            if($validator->fails())
                throw new ValidationException($validator);
        }
    }
}