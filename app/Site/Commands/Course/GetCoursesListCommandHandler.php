<?php namespace Site\Commands\Course;

use Category;
use Course;
use Group;
use Illuminate\Database\Query\Builder;
use Laracasts\Commander\CommandHandler;

class GetCoursesListCommandHandler implements CommandHandler {

    /**
     * Handle the command.
     *
     * @param GetCoursesListCommand $command
     *
     * @return \Illuminate\Pagination\Paginator|mixed
     */
    public function handle($command)
    {
        $activeCategoryTitle = null;
        $activeSubCategoryTitle = null;
        $categoriesForCourses = null;

        if($command->subCategoryTitle)
        {
            $categoriesForCourses = Category::where('seo_title', $command->subCategoryTitle);
        }
        elseif( ! $command->subCategoryTitle and $command->categoryTitle)
        {
            $categoriesForCourses = Category::where('parent_id', Category::where('seo_title', $command->categoryTitle)->lists('id'));
        }

        $courses = Course::with([
            'groups' => function($query)
            {
                /**
                 * @var Builder $query
                 */
                $query->orderBy('begins');
            },
            'age',
            'level',
            'category',
            'school'
        ])
            ->where(function($query) use ($command, $categoriesForCourses)
            {
                /**
                 * @var Builder $query
                 */
                if($categoriesForCourses)
                    $query->whereIn('category_id', array_values($categoriesForCourses->lists('id')));

                if($command->age)
                    $query->where('age_id', $command->age);

                if($command->level)
                    $query->where('level_id', $command->level);
            })
            ->whereHas('groups', function($query) use ($command)
            {
                /**
                 * @var Builder $query
                 */
                if($command->price_from or $command->price_to)
                {
                    $query->whereBetween('price', [$command->price_from, $command->price_to]);
                }
                $query->where('status', 1);
            }, '>=', 1)
            ->orderBy('id', 'desc')
            ->paginate(12)
        ;

        $activeCategory = null;
        if($command->subCategoryTitle)
        {
            $activeCategory = Category::with('parent')->where('seo_title', $command->subCategoryTitle)->first();
        }
        elseif($command->categoryTitle and ! $command->subCategoryTitle)
        {
            $activeCategory = Category::where('seo_title', $command->categoryTitle)->first();
        }

        $category = null;
        $subCategories = [];
        if($command->subCategoryTitle)
        {
            $category = $activeCategory->parent;
        }
        elseif( ! $command->subCategoryTitle and $command->categoryTitle)
        {
            $category = $activeCategory;
        }

        if($category)
            $subCategories = $category->childrens;

        $prices = [];
        foreach($courses as $course)
        {
            foreach($course->groups as $group)
            {
                $prices[] = $group->price;
            }
        }

        sort($prices);
        $prices = [
            'price_from' => intval(head($prices)),
            'price_to' => intval(last($prices)),
        ];

        return compact('courses', 'command', 'prices', 'category', 'activeCategory', 'subCategories');
    }

}