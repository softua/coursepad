<?php namespace Site\Commands\Course;

use Client;
use Group;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use Order;

class BuyCourseCommandHandler implements CommandHandler {

    use DispatchableTrait;

    /**
     * Handle the command
     *
     * @param BuyCourseCommand $command
     *
     * @return mixed
     */
    public function handle($command)
    {
        /**
         * @var Group $group
         */
        $group = Group::findOrFail($command->group_id);

        $payer = Client::createPayer($command);

        $listeners = [];

        foreach($command->listeners as $listener)
        {
            $listeners[] = Client::createListener($listener);
        }

        $countListeners = count($listeners);
        if($command->me_will_go)
            $countListeners++;

        $order = Order::createNewOrder($group, $payer, $listeners, $countListeners);

        $group->seats_busy += $countListeners;
        $group->save();
        $group->orders()->save($order);

        $this->dispatchEventsFor($order);

        return $order;
    }}