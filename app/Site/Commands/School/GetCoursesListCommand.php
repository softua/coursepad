<?php namespace Site\Commands\School;

class GetCoursesListCommand {

    /**
     * @var null
     */
    public $age;

    /**
     * @var null
     */
    public $level;

    /**
     * @var null
     */
    public $price_from;

    /**
     * @var null
     */
    public $price_to;

    /**
     * @var null
     */
    public $metro;

    /**
     * @var null
     */
    public $categoryTitle;
    /**
     * @var null
     */
    public $subCategoryTitle;

    /**
     * @var
     */
    public $schoolTitle;
    /**
     * @var int
     */
    public $limit;
    /**
     * @var bool
     */
    public $withDisabled;

    /**
     * @param null $age
     * @param null $level
     * @param null $price_from
     * @param null $price_to
     * @param null $metro
     * @param null $categoryTitle
     * @param null $subCategoryTitle
     * @param      $schoolTitle
     * @param bool $withDisabled
     * @param int  $limit
     */
    public function __construct(
        $age = null,
        $level = null,
        $price_from = null,
        $price_to = null,
        $metro = null,
        $categoryTitle = null,
        $subCategoryTitle = null,
        $schoolTitle,
        $withDisabled = false,
        $limit = 12)
    {
        $this->age              = $age;
        $this->level            = $level;
        $this->price_from       = $price_from;
        $this->price_to         = $price_to;
        $this->metro            = $metro;
        $this->categoryTitle    = $categoryTitle;
        $this->subCategoryTitle = $subCategoryTitle;
        $this->schoolTitle      = $schoolTitle;
        $this->limit            = $limit;
        $this->withDisabled     = $withDisabled;
    }
}