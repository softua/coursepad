<?php namespace Site\Exceptions;

use Illuminate\Validation\Validator;

class ValidationException extends \Exception {

    /**
     * Errors object.
     *
     * @var
     */
    private $errors;

    /**
     * Create a new validate exception instance.
     *
     * @param  Validator $container
     *
     * @return \Site\Exceptions\ValidationException
     */
    public function __construct($container)
    {
        $this->errors = ($container instanceof Validator) ? $container->errors() : $container;

        parent::__construct(null);
    }

    /**
     * Gets the errors object.
     *
     * @return Validator
     */
    public function getErrors()
    {
        return $this->errors;
    }


}