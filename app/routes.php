<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::any('ajax/image', ['as' => 'ajax-image', 'uses' => 'AjaxController@getImage']);
Route::any('ajax/image/course', 'AjaxController@getImageCourse');
Route::post('/ajax/group/add/{courseId}', 'AjaxController@addGroup');
Route::post('/ajax/group/edit/{groupId}', 'AjaxController@editGroup');
Route::post('/ajax/group/delete/{groupId}', 'AjaxController@deleteGroup');

// Password
Route::get('password/reminder', 'RemindersController@getRemind');
Route::post('password/reminder', 'RemindersController@postRemind');
Route::get('password/reset/{token}', 'RemindersController@getReset');
Route::post('password/reset/', 'RemindersController@postReset');

// User
Route::any('/login', ['as' => 'login', 'uses' => 'UserController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
Route::get('/register', ['as' => 'register', 'uses' => 'UserController@register']);
Route::post('/register', 'UserController@postRegister');
Route::get('/activation/{userId}/{activationCode}', 'UserController@getActivate');

// School
Route::get('/course/add', [
	'before' => 'auth',
	'as' => 'course-add',
	'uses' => 'SchoolController@getAddCourse'
]);
Route::post('/course/add', [
	'before' => 'auth',
	'uses' => 'SchoolController@postAddCourse'
]);
Route::get('/course/edit/{title}', [
	'before' => 'auth|editable_course|isset_course',
	'as' => 'course-edit',
	'uses' => 'SchoolController@getEditCourse'
]);
Route::post('/course/edit/{title}', [
	'before' => 'auth|editable_course|isset_course',
	'uses' => 'SchoolController@postEditCourse'
]);
Route::get('school/{title}', [
	'as' => 'school',
	'uses' => 'SchoolController@index'
]);

// Courses
Route::get('course/{title}', ['as' => 'course', 'uses' => 'CoursesController@show']);
Route::get('course/buy/{groupId}', [
	'as' => 'buy',
	'uses' => 'CoursesController@getBuy'
]);
Route::post('course/buy', ['as' => 'course-buy', 'uses' => 'CoursesController@postBuy']);
Route::get('catalog/{categoryTitle?}/{subcategoryTitle?}', ['as' => 'catalog', 'uses' => 'CoursesController@index']);

// Оплата
Route::post('payment/result', [
	'uses' => 'PaymentController@result'
]);
Route::post('payment/success', [
	'uses' => 'PaymentController@success'
]);
Route::post('payment/fail', [
	'uses' => 'PaymentController@fail'
]);

// Home
Route::get('/', ['as' => 'home', 'uses' => 'CoursesController@index']);
