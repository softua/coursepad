var GetCatalog = {
	DOM:
	{
		control:'a.changeUrl' // контрол смены урла
	},
	init:function(){
		var self = this;
		$(self.DOM.control).on('click',function(e){
			e.preventDefault();
			var link = $(this).attr('href');
			History.pushState({state:link},link,link);
		})
	}
};

$(document).ready(function(){

	$("#inputPrice").slider({tooltip: 'hide'});
	$("#inputPrice").on("slide", function(slideEvt) {
		$(".priceFrom").text(slideEvt.value[0]);
		$("#price_from").val(slideEvt.value[0]);
		$(".priceTo").text(slideEvt.value[1]);
		$("#price_to").val(slideEvt.value[1]);
	});

	var speed = 300; //скорость анимции
	var page_url = window.location.href; //входящий урлик

//#################### Отображение подкатегорий ############################

	//показать/скрыть меню подкатегорий
	$('#subjects-menu a').on('click',function(e){
		e.preventDefault();
		if($(this).parent().hasClass('opens-sub')){
			$(this).parent().removeClass('opens-sub');
			destructSubList();
		}
		else
		{
			$('#sidebar').find('li.dropdown.opens-sub').removeClass('opens-sub');
			$(this).parent().addClass('opens-sub');
			var subcats = $(this).next().html();
			destructSubList()
			constructSubList(subcats);
		}
		$('body').toggleClass('modal-open');
		setSubmenuSizeObj();
		return false;
	});

	//построение меню подкатегрий
	function constructSubList(data){
		var block = $('<ul id="subcats_list" class="dropdown-menu" role="menu" style="position:fixed; left: 266px; width: 929px; height: 899px;"></ul>').append(data);
		block.css('display','block').css('top',$('#header').height());
		$('#page-body').append(block);
		GetCatalog.init();
	}

	//удаление меню подкатегорий
	function destructSubList(){
		$('#subcats_list').remove();
	}

	//установка геометрических параметров построеного меню подкатегорий
	function setSubmenuSizeObj(){
		var subMenu  = $('#subcats_list');
		if(subMenu.length){
			subMenu.parent().css({marginRight:'-1px'});
			var sidebarWidth = $('#sidebar').width()-1;
			subMenu.css({left:sidebarWidth,width:$('body').width() - sidebarWidth,height:$(window).height()-$('#header').height()});
		}
	}

//################################ END #####################################
});