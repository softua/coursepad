var counter = 0;
$(document).ready(function() {
    $('#me_will_go').on('change', function() {
        renderPrice();
    });
    //renderListeners();
    $('.addListener-btn').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        renderListeners();
    });
    renderPrice();
    $('#group').on('change', function() {renderPrice()});
});
/**
 * Проверка на то, отмечена ли галочка "Я пойду на курс" при оплате
 * @returns {boolean}
 */
function checkWithMe() {
    return !!$('#me_will_go').is(':checked');
}
function renderListeners() {
    counter++;
    var pattern = $('.listener-pattern')
        , listenerBlock = pattern.clone()
        , labels = listenerBlock.find('.listenerLabel')
        , inputs = listenerBlock.find('input')
        , listenersContainer = $('#listeners')
        ;
    labels.each(function() {
        var labelText = $(this).text();
        $(this).text(labelText.replace('{%listenerId%}', counter));
        $(this).attr('for', labelText.replace('{%listenerId%}', counter));
    });
    inputs.each(function() {
        var name = $(this).attr('name')
            , id = $(this).attr('id')
            ;
        $(this).attr('name', name.replace('{%listenerId%}', counter));
        $(this).attr('id', id.replace('{%listenerId%}', counter));
        $(this).removeAttr('disabled');
    });
    listenerBlock.removeClass('listener-pattern').addClass('listener');
    listenersContainer.append(listenerBlock);
    listenersContainer.find('.delete-listener-btn').addClass('sr-only');
    listenersContainer.find('.listenerNumberNamePattern').removeClass('listenerNumberNamePattern').addClass('listenerNumberName');
    listenersContainer.children().last().find('.delete-listener-btn').removeClass('sr-only');
    listenerBlock.find('.delete-listener-btn', listenerBlock).on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        listenerBlock.remove();
        listenersContainer.children().last().find('.delete-listener-btn').removeClass('sr-only');
        counter--;
        renderPrice();
    });
    renderPrice();
}
function renderPrice() {
    var priceContainer = $('.course-group-closest-price').find('span')
        , groupId = $('#group').val()
        , listeners = (
                          checkWithMe()
                      ) ? counter + 1 : counter
        , placesContainer = $('.course-group-closest-places').find('strong')
        ;
    var groupData = {};
    $.each(groupsData, function(index, obj) {
        if(obj.id == groupId) {
            groupData = obj;
        }
    });
    var coursePrice = groupData.price
        , places = groupData.seats_left - listeners;
    $('.data-group-listeners').text(listeners + ' ' + plural_str(listeners, 'участник', 'участника',
        'участников'));
    $('.data-group-price').text(coursePrice + ' руб');
    var listenersNumber = 0;
    if(checkWithMe()) {
        listenersNumber = 1;
    }
    $('.listenerNumberName').each(function() {
        $(this).text('Участник №' + listenersNumber);
        listenersNumber++;
    });
    priceContainer.text(coursePrice * listeners);
    placesContainer.text(places + ' ' + plural_str(places, 'место', 'места', 'мест'));
}
function plural_str(i, str1, str2, str3) {
    function plural(a) {
        if(a % 10 == 1 && a % 100 != 11) {
            return 0
        } else {
            if(a % 10 >= 2 && a % 10 <= 4 && (
                a % 100 < 10 || a % 100 >= 20
                )) {
                return 1
            } else {
                return 2;
            }
        }
    }

    switch(plural(i)) {
        case 0:
            return str1;
        case 1:
            return str2;
        default:
            return str3;
    }
}
