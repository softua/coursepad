init:function(){
			var self = this;
			$(self.DOM.control).on('click',function(e){
				e.preventDefault();
				var link = $(this).attr('href');
				$('#subcats_list').remove();
				$('.dropdown').removeClass('opens-sub');
				History.pushState({state:link},link,link);
				$.ajax({
					type: 'POST',
					url: "/login",
					data: link,
					success: function(result){
					}
				});
			})
		}
		$(self.DOM.submit).on("click",function(e){
				e.preventDefault();
				$(self.DOM.target).find('div.input-group').each(function(){
					var name = this.getAttribute("group");
					var value = '';
					$(this).children().each(function(){
						value = value + this.value+';';
					});
					$(self.DOM.data).append('<input type="hidden" id="group_'+name+'" name="group_'+name+'" value="'+value+'">');
				});
				$(self.DOM.form).submit();
			});