//для загрузки картинок
var choose = document.getElementById('choose');
var imagesID = '';
FileAPI.event.on(choose, 'change', function (evt){
	var files = FileAPI.getFiles(evt); 
	
	FileAPI.each(files, function (file){
		FileAPI.Image(file).resize(320, 240, 'max');
	});
	
	FileAPI.upload({
		url: '/ajax/image/course',
		files: { images: files },
		progress: function (evt/**Object*/, file/**Object*/, xhr/**Object*/, options/**Object*/){
			var pr = evt.loaded/evt.total * 100;
			if(pr<100){
				$('div.loader').css('display','block');
			}
			else{
				$('div.loader').css('display','none');
			}
		},
		filecomplete: function (err, xhr, file, options){
			if( !err )
			{
				var result = jQuery.parseJSON(xhr.responseText);
				FileAPI.Image(file)
					.preview(150, 150)
					.get(function (err, img){
					sortable.appendChild(img);
					$(img).wrap('<li class="ui-state-default" id="id-image_'+result.id+'"></li>');
					if(imagesID.length == 0)
					{
						imagesID = imagesID + result.id;
					}
					else
					{
						imagesID = imagesID + ','+result.id;
					}
					$('input.images').val(imagesID);
					DeleteImage.init(img);
				});
			}
		}
	});
});

//удаление картинок
var DeleteImage = {
	init:function(obj){
		$(obj).on('dblclick',function(){
			$(this).parent().remove();
			var imagesID = '',id;
			$('ul.ui-sortable').find('li.ui-state-default').each(function(){
				id = this.getAttribute("id").split('_');
				if(imagesID.length == 0)
				{
					imagesID = imagesID + id[id.length-1];
				}
				else
				{
					imagesID = imagesID + ','+id[id.length-1];
				}
			});
			$('input.images').val(imagesID);
		});
	}
};

//сортировка картинок
$(function() {
	$( "#sortable" ).sortable({ distance: 5,forceHelperSize: true });
	$( "#sortable" ).disableSelection();
	$( "#sortable" ).on( "sortupdate", function( event, ui ) {
		$(this).sortable( "refreshPositions" );
		var images = '',id;
		$('ul.ui-sortable').find('li.ui-state-default').each(function(){
			id = this.getAttribute("id").split('_');
			images = images + id[id.length-1]+',';
		});
		$('input.images').val(images);
	});
});

//карта
ymaps.ready(init);

function init() {
	
	//Создание карты
	var myMap = new ymaps.Map('map', {
		center: [55.76, 37.64], // Москва
        zoom: 12,
		controls: []
	});
	
	
	$('.search_address').typeahead({
		source: function(query,process)
		{
			var AutocompleteAddr = [];
			var callback = function(event){
				if(event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 13 )
				{
					return false;
				}
				
				myMap.geoObjects.removeAll();
				var request = 'Россия, Москва '+this.value;
				ymaps.geocode(request, {
					results: 10,
					json:true
				}).then(function (res) {
					
					var allobjects = res.GeoObjectCollection.featureMember,GeoObjAddress;
					for (var i = 0, l = allobjects.length; i < l; i++) {
						GeoObjAddress = allobjects[i].GeoObject.metaDataProperty.GeocoderMetaData.text;
						AutocompleteAddr.push(GeoObjAddress);
					};
					process(AutocompleteAddr);
				});
			};
			$('input.search_address').on('keyup',callback);
		}
	});
	
	//Показать текущий аддрес на карте(едактирование курса)
	$('input.search_address').val($('input#course_address').val());
	ymaps.geocode($('input#course_address').val(), {
			results: 1
		}).then(function (res) {
				var firstGeoObject = res.geoObjects.get(0),
					coords = firstGeoObject.geometry.getCoordinates(),
					myPlacemark = new ymaps.Placemark(coords, {
						balloonContentHeader: firstGeoObject.properties.get('name'),
						balloonContentBody: firstGeoObject.properties.get('text')
					});
				myMap.geoObjects.add(myPlacemark);
				myMap.setCenter(coords, 13, {
					checkZoomRange: true
				});
				findMettro(coords);
			});
	
	//Создать объект поисковика
	var searchControl = new ymaps.control.SearchControl({ provider: 'yandex#publicMap' });
	myMap.controls.add('zoomControl');
	
	$('input.search_address').on('keyup',function(){
		var thisObj = this;
		myMap.geoObjects.removeAll();
		var request = 'Россия, Москва '+this.value;
		ymaps.geocode(request, {
			results: 1
		}).then(function (res) {
				var firstGeoObject = res.geoObjects.get(0),
					coords = firstGeoObject.geometry.getCoordinates(),
					bounds = firstGeoObject.properties.get('boundedBy'),
					myPlacemark = new ymaps.Placemark(coords, {
						balloonContentHeader: firstGeoObject.properties.get('name'),
						balloonContentBody: firstGeoObject.properties.get('text')
					});
				myMap.geoObjects.add(myPlacemark);
				myMap.setBounds(bounds, {
					checkZoomRange: true
				});
				findMettro(coords);
				$('input#course_address').val(firstGeoObject.properties.get('text'));
			});
	});
	
	
	// Слушаем клик на карте
	myMap.events.add('click', function (e) {
		myMap.geoObjects.removeAll();
		searchControl.hideResult();
		var coords = e.get('coords');
		myPlacemark = new ymaps.Placemark(coords);
		myMap.geoObjects.add(myPlacemark);
		
		// Слушаем событие окончания перетаскивания на метке.
		myPlacemark.events.add('dragend', function () {
			getAddress(myPlacemark.geometry.getCoordinates());
		});
		var address = getAddress(coords);
	});

	// Определяем адрес по координатам (обратное геокодирование)
	function getAddress(coords) {
		ymaps.geocode(coords).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0);
			myPlacemark.properties
				.set({
					balloonContentHeader: firstGeoObject.properties.get('name'),
					balloonContentBody: firstGeoObject.properties.get('text')
				});
				$('input#course_address').val(firstGeoObject.properties.get('text'));
				$('input.search_address').val(firstGeoObject.properties.get('text'));
		});
		findMettro(coords)
	}

	function findMettro(coords){
		// Поиск станций метро.
		ymaps.geocode(coords, {
			kind: 'metro',
			results: 1
		}).then(function (res) {
			// Задаем изображение для иконок меток.
			res.geoObjects.options.set('preset', 'islands#redCircleIcon');
			// Добавляем коллекцию найденных геообъектов на карту.
			myMap.geoObjects.add(res.geoObjects);
			$('input#metro_address').val(res.geoObjects.get(0).properties.get('text'));
		});
	}
}


$(document).ready(function(){
	window.onload = function()
	{
		CKEDITOR.config.fullPage = false;
		CKEDITOR.config.language = 'ru';
		CKEDITOR.config.defaultLanguage = 'ru';
		CKEDITOR.replace( 'full_description',{
			
			filebrowserBrowseUrl: '/js/ckeditor/plugins/fileman/index.html',
			filebrowserImageBrowseUrl: '/js/ckeditor/plugins/fileman/index.html?type=image',
			toolbar: [
						{ name: 'basicstyles', items: [ 'Format','Bold', 'Italic','Strike','NumberedList','BulletedList','Image','Table','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','HorizontalRule' ] }
					]
		});
	}
})