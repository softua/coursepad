//запуск баннера
jQuery(function($) {
	jQuery("#banner .banner").show().revolution({
		startwidth:1170,
		startheight:682,
		delay:8000,
		hideThumbs:10,
		hideTimerBar:"on",
		navigationType:"none",
		hideArrowsOnMobile:"on",
		touchenabled:"on",
		onHoverStop:"on",
		navOffsetHorizontal:0,
		navOffsetVertical:20,
		stopAtSlide:-1,
		stopAfterLoops:-1,
		shadow:0,
		fullWidth:"on",
		fullScreen:"off"
	});
	$("#discipline .more").popover();
});
VK.Widgets.Group("vk_groups", {mode: 0, width: "212", height: "330", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 60203367);