var UserLogin = function(){
	return({
		DOM:
		{
			form:'form#ulogin',
			email:'input#uloginemail',
			password:'input#uloginpassword',
			submit:'button#ulogin-submit',
			target:'.user-login-logout-control'
		},
		login:function(){
			var self = this;
			var datatosend = {};
			datatosend.email = $(self.DOM.email).val();
			datatosend.password = $(self.DOM.password).val();
			$.ajax({
				type: 'POST',
				url: "/login",
				data: datatosend,
				success: function(result){
					if(result.result)
					{
						$('.bs-example-modal-sm').modal('hide');
						location.reload();
					}
					else
					{
						$('p.help-block').show();
					}
				}
			});
		},
		init:function(){
			var self = this;
			$(self.DOM.form).submit(function(e){
				e.preventDefault();
				self.login();
// 				return false;
			});
			$(self.DOM.submit).bind('click',function(){
				self.login();
			})
		}
	})
}
$(document).ready(function(){
	var userlogin = new UserLogin();
	userlogin.init();
});