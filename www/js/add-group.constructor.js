//Добавление группы. Редактирование группы.
var AddGroup = function(){
	return({
		DOM:
		{
			control:'.add-group-control', //контрол добавить новую группу
			target:'div.groups-target', //блок куда запихивать поля для новой группы
			del_group:'span.del-group', // контрол удалить контейнер с новой группой
			save_group:'span.ok-group', // контрол отправить аякс с новой группой
		},
		/*Собирает данне редактируемой группы*/
		getDataDefinedGroup:function(self,obj){
			$('.groups-list').on('click','.save-defined-group',function(){
				//собираем данне
				var datatosend = {},thisobj;
				$(this).closest("tr").find('td.changeable').each(function(){
					thisobj = $(this).find('input')[0];
					datatosend[thisobj.getAttribute('name')] = thisobj.value;
				});
				self.sendRequestEdit(self,datatosend,obj.origin,obj.href,obj.id_group);
			});
		},
		/*Собирает данне добавляемой группы*/
		getData:function(self,obj){
			$(self.DOM.save_group).on('click',function(){
				var datatosend = {};
				$(this).parent().find('input').each(function(){
					datatosend[this.getAttribute('name')] = this.value
				});
				datatosend.course_id = obj.id;
				
				//шлем на сервер
				self.sendRequest(self,datatosend,obj.href,this);
			});
		},
		/*Отправляет и обрабатывает данне добавляемой группы*/
		sendRequest:function(self,data,href,thisobj){
			var table = $(thisobj).closest('div.row').find('table.groups-list')[0];
			$(thisobj).parent().parent().remove();
			$.ajax({
				type:'POST',
				url:href,
				data:data,
				success:function(result){
					if(result.id)
					{
						$(table).append('<tr data-group-id="'+result.id+'" id="group_'+result.id+'"><td class="text-center changeable" input-name="date_start">'+data.date_start+'</td><td class="text-center changeable" input-name="lessons_count">'+data.lessons_count+'</td><td class="text-center changeable" input-name="place_count">'+data.place_count+'</td><td class="text-center changeable" input-name="price">'+data.price+'</td><td class="text-center group-controls"><span class="glyphicon glyphicon-remove del-defined-group" data-delete-href="'+result.url_delete+'"></span><span class="glyphicon glyphicon-pencil ok-defined-group" id="edit_control_group_'+result.id+'" data-edit-href="'+result.url_edit+'"></span><span class="glyphicon glyphicon-ok save-defined-group"></span></td></tr>');
					}
					else
					{
						for (var i=0; i < result.errors.length; i++) {
								alert(result.errors[i]);
							}
					}
				}
			});
		},
		/*Отправляет и обрабатывает данне редактируемой группы*/
		sendRequestEdit:function(self,data,origin,href,id){
			var complete = 0;
			for (var key in origin) {
				if(origin[key] != data[key])
				{
					complete = 1;
					break;
				}
			}
			if(complete)
			{
				var thisobj;
				$.ajax({
					type:'POST',
					url:href,
					data:data,
					success:function(result){
						if(!result.errors)
						{
// 							alert();
							//редактируемые поля в текст с новыми значеними
							$('#group_'+id).find('td.changeable').each(function(){
								inputName = this.getAttribute('input-name');
								$(this).html(data[inputName]);
							});
						}
						else
						{
							for (var i=0; i < result.errors.length; i++) {
								alert(result.errors[i]);
							}
							$('#group_'+id).find('td.changeable').each(function(){
								inputName = this.getAttribute('input-name');
								$(this).html(origin[inputName]);
							});
						}
					}
				});
			}
			else
			{
				$('#group_'+id).find('td.changeable').each(function(){
					inputName = this.getAttribute('input-name');
					$(this).html(origin[inputName]);
				});
			}
			thisobj = document.getElementById('edit_control_group_'+id);
			$(thisobj).css('display','inline-block');
			$(thisobj).next().css('display','none');
			if($(thisobj).prev().attr('class') == 'glyphicon glyphicon-remove del-defined-group')
			{
				$(thisobj).prev().css('display','inline-block');
			}
		},
		init:function(){
			var self = this;
			var counter = 1, editcounter = 100;
			var obj;
			
//######################удаляем группу#########################
			
			$('.groups-list').on("click",'.del-defined-group',function(){
				var href = this.getAttribute('data-delete-href'), thisobj = this;
				$.ajax({
					type:'POST',
					url:href,
					success:function(result){
						if(result.result)
						{
							$(thisobj).closest("tr").remove();
							//удаляем с html
						}
						else
						{
							for (var i=0; i < result.errors.length; i++) {
								alert(result.errors[i]);
							}
						}
					}
				});
			});
			
//##################################################################
			
//#####################добавляем группу#########################
			
			$(self.DOM.control).on("click",function(){
				obj = {};
				obj.id = this.getAttribute('data-course-id');
				obj.href = this.getAttribute('data-add-href');
				$(this).parent().find(self.DOM.target).append('<div class="form-group"><hr><div class="input-group" group="'+counter+'"><input placeholder="Дата начала" id="datepicker_'+counter+'" class="input-xxlarge form-control datepicker" style="width:20%;margin:0px 1%" type="text" name="date_start" /><input placeholder="Количество уроков" style="width:20%;margin:0px 1%" id="group_'+counter+'_lessons" class="input-xxlarge form-control" name="lessons_count" type="text"><input placeholder="Количество мест" style="width:20%;margin:0px 1%" id="group_'+counter+'_lessons" class="input-xxlarge form-control" name="place_count" type="text"><input placeholder="Цена" style="width:20%;margin:0px 1%" id="group_'+counter+'_price" class="input-xxlarge form-control" name="price" type="text"><span class="glyphicon glyphicon-remove del-group"></span><span class="glyphicon glyphicon-ok ok-group" id="group_'+counter+'" ></span></div></div> ');
				jQuery('#datepicker_'+counter).datetimepicker({
					lang:'ru',
					format:'d.m.Y H:i'
				});
				counter++;
				//удалить групу с html
				$(self.DOM.del_group).on("click",function(){
					$(this).parent().parent().remove();
				});
				
				//собираем данные по группе для запроса
				self.getData(self,obj);
			});
			
//##################################################################
			
//#####################редактируем группу#########################
			
			$('.groups-list').on('click','.ok-defined-group',function(){
				obj = {origin:{}};
				var value,inputName,container;
				
				//добавим контрол сохранения
				container = $(this).closest("tr");
				$(this).next().css('display','inline-block');
				if($(this).prev().attr('class') == 'glyphicon glyphicon-remove del-defined-group')
				{
					$(this).prev().css('display','none');
				}
				//превращаем текст в редактируемые поля
				$(container).find('td.changeable').each(function(){
					value = $(this).html();
					inputName = this.getAttribute('input-name');
					obj.origin[inputName] = value;
					if(inputName == 'date_start')
					{
						$(this).html('<input id="datepicker_'+editcounter+'" class="input-xxlarge form-control datepicker" style="width:100%;padding: 5px;text-align:center;margin:0px 1%" type="text" name="'+inputName+'" value="'+value+'" />');
					}
					else
					{
						$(this).html('<input class="input-xxlarge form-control datepicker" style="width:100%;padding: 5px;text-align:center;margin:0px 1%" type="text" name="'+inputName+'" value="'+value+'" />');
					}
					jQuery('#datepicker_'+editcounter).datetimepicker({
						lang:'ru',
						format:'d.m.Y H:i'
					});
					editcounter++;
				});
				obj.id_group = $(container).attr('data-group-id');
				obj.savecontrol = '.save-defined-group';
				obj.href = this.getAttribute('data-edit-href');
				self.getDataDefinedGroup(self,obj);
				$(this).css('display','none');
			});
			
//##################################################################
			
		}
	})
}
$(document).ready(function(){
	var addgroup = new AddGroup();
	addgroup.init();
});
