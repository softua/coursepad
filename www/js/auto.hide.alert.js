var HideAlert = function(){
	return({
		init:function(){
			setTimeout(function(){
				$(".alert").alert('close');
			}, 15000);
		}
	});
};
$(document).ready(function(){
	var hidealert = new HideAlert();
	hidealert.init();
});