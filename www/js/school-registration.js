var FormValidation = function(){
	return({
		DOM:
		{
			form:'form-vertical',
			title:'input#title',
			email:'input#email',
			phone:'input#phone',
			name:'input#name',
			password:'input#password-reg',
			password_confirm:'input#password_confirmation',
			Error:'p.help-block',
			button:'input#btn-register'
		},
		check:function(obj,length,maxlength){
			var self = this;
			var msg = 'Поле должно содержать не менее 2 символов';
			if(obj.name == 'email')
			{
				msg = 'Поле '+obj.name+' обязательно для заполнения';
			}
			else if(obj.name == 'password' || obj.name == 'password_confirmation')
			{
				msg = 'Поле '+obj.name+' должно быть от 5 до 20 символов';
			}
			if(maxlength)
			{
				if (obj.value.length < length && obj.value.length < maxlength)
				{
					$(obj).next().html(msg);
					$(obj).next().show();
					$(self.DOM.button).attr('disabled','disabled');
					return 0;
				}
				else
				{
					$(obj).next().html('');
					$(obj).next().hide();
					$(self.DOM.button).removeAttr('disabled');
					return 1;
				}
			}
			else if (obj.value.length < length)
			{
				$(obj).next().html(msg);
				$(obj).next().show();
				$(self.DOM.button).attr('disabled','disabled');
				return 0;
			}
			else
			{
				$(obj).next().html('');
				$(obj).next().hide();
				$(self.DOM.button).removeAttr('disabled');
				return 1;
			}
		},
		comparepass:function(pass,conf){
			var self = this;
			if(pass == conf)
			{
				$(self.DOM.password_confirm).next().html('');
				$(self.DOM.password_confirm).next().hide();
				$(self.DOM.button).removeAttr('disabled');
			}
			else
			{
				$(self.DOM.password_confirm).next().html('пароли не совпадают');
				$(self.DOM.password_confirm).next().show();
				$(self.DOM.button).attr('disabled','disabled');
			}
		},
		init:function(){
			var self = this;
			
			//check phone
			$(self.DOM.phone).bind( "keyup change", function() {
				var phoneReplace = /\D/gi;
				var phoneCheck = /^[0-9]+$/;
				this.value = this.value.replace(phoneReplace,"");
				if (!phoneCheck.test(this.value))
				{
					$(this).next().html('Поле '+this.name+' может содержать только цифры');
					$(this).next().show();
					$(self.DOM.button).attr('disabled','disabled');
				}
				else
				{
					$(this).next().hide();
					$(self.DOM.button).removeAttr('disabled');
				}
			});
			
			//check title
			$(self.DOM.title).bind( "keyup change", function() {
				var titleCheck = /^[а-яА-Яa-zA-Z0-9- _]+$/;
				if (!titleCheck.test(this.value))
				{
					$(this).next().html('Поле '+this.name+' может содержать только буквы, цифры, дефис или пробел');
					$(this).next().show();
					$(self.DOM.button).attr('disabled','disabled');
				}
				else
				{
					self.check(this,2);
				}
			});
			
			//check name
			$(self.DOM.name).bind( "keyup change", function() {
				var nameCheck = /^[а-яА-Яa-zA-Z0-9- _]+$/;
				if (!nameCheck.test(this.value))
				{
					$(this).next().html('Поле '+this.name+' может содержать только буквы, цифры, дефис или пробел');
					$(this).next().show();
					$(self.DOM.button).attr('disabled','disabled');
				}
				else
				{
					$(this).next().hide();
					$(self.DOM.button).removeAttr('disabled');
				}
			});
			
			//check email
			$(self.DOM.email).bind( "keyup change", function() {
				var emailCheck = /^[-._a-z0-9]+@([-a-z0-9]+\.)+[a-z]{2,6}$/i;
				if(self.check(this,1))
				{
					if (!emailCheck.test(this.value))
					{
						$(this).next().html('Поле '+this.name+' должно быть действительным электронным адресом' );
						$(this).next().show();
						$(self.DOM.button).attr('disabled','disabled');
					}
				}
			});
			
			//check password
			$(self.DOM.password).bind( "keyup change", function() {
				if(self.check(this,5,20))
				{
					self.comparepass($(this).val(), $(self.DOM.password_confirm).val());
				}
			});
			$(self.DOM.password_confirm).bind( "keyup change", function() {
				self.comparepass($(self.DOM.password).val(), $(self.DOM.password_confirm).val());
			});
		}
	})
};

//удаление картинки
var DeleteImage = {
	init:function(obj){
		$(obj).on('dblclick',function(){
			$(this).remove();
			$('input.image_id').val('');
		});
	}
}
$(document).ready(function(){
	var formvalidation = new FormValidation();
	formvalidation.init();
	
	var choose = document.getElementById('choose');
	FileAPI.event.on(choose, 'change', function (evt){
		var files = FileAPI.getFiles(evt); 
		var imageFile = files[0];
		
		FileAPI.Image(imageFile)
			.resize(320, 240, 'max')
			.preview(150, 150)
			.get(function (err, img){
			$(images).html('');
			images.appendChild(img);
			DeleteImage.init(img);
		});
			
		FileAPI.upload({
			url: '/ajax/image',
			files: { images: imageFile },
			filecomplete: function (err, xhr, file, options){
				if( !err ){
					// Файл загружен успешно
					var result = jQuery.parseJSON(xhr.responseText);
					$('input.image_id').val(result.id);
				}
			}
		});
	});
});