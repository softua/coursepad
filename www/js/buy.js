var FormValidation = {
	DOM:
	{
		form:'form.form-inline',
		email:'input#email',
		phone:'input#tel',
		name:'input#name',
		Error:'p.help-block',
		button:'input#btn-register',
		globalComplete:1
	},
	check:function(obj,length,maxlength){
		var self = this, msg = 'Поле '+obj.name+' обязательно для заполнения';
		if (obj.value.length < length)
		{
			$(obj).next().html(msg);
			$(obj).next().show();
			self.DOM.globalComplete = 0;
			return 0;
		}
		else
		{
			$(obj).next().html('');
			$(obj).next().hide();
			self.DOM.globalComplete = 1;
			return 1;
		}
	},
	init:function(){
		var self = this;
		
		//check phone
		$(self.DOM.phone).bind( "keyup change", function() {
			var phoneReplace = /\D/gi;
			var phoneCheck = /^[0-9]+$/;
			this.value = this.value.replace(phoneReplace,"");
			if (!phoneCheck.test(this.value))
			{
				$(this).next().html('Поле '+this.name+' может содержать только цифры');
				$(this).next().show();
				self.DOM.globalComplete = 0;
			}
			else
			{
				$(this).next().hide();
				self.DOM.globalComplete = 1;
			}
		});
		
		//Check name
		$(self.DOM.name).bind( "keyup change", function() {
			var nameCheck = /^[а-яА-Яa-zA-Z0-9- _]+$/;
				if(self.check(this,2))
				{
					if (!nameCheck.test(this.value))
					{
						$(this).next().html('Поле '+this.getAttribute('placeholder')+' может содержать только буквы, цифры, дефис или пробел');
						$(this).next().show();
						self.DOM.globalComplete = 0;
					}
					else
					{
						$(this).next().hide();
						self.DOM.globalComplete = 1;
					}
				}

		});
		
		//Check email
		$(self.DOM.email).bind( "keyup change", function() {
			var emailCheck = /^[-._a-z0-9]+@([-a-z0-9]+\.)+[a-z]{2,6}$/i;
			if(self.check(this,1))
			{
				if (!emailCheck.test(this.value))
				{
					$(this).next().html('Поле '+this.getAttribute('placeholder')+' должно быть действительным электронным адресом' );
					$(this).next().show();
					self.DOM.globalComplete = 0;
				}
			}
		});
		
		//Check fields for emptiness
		$(self.DOM.form).on('submit',function(event){
			var complete = 1, listenerComplete = 1;
			
			//Check email and name for emptiness
			$(this).find('.check').each(function(){
				if(!this.value)
				{
					complete = 0;
					$(this).next().html('Поле '+this.getAttribute('placeholder')+' обьязательно для заполнения' );
					$(this).next().show();
					return false;
				}
			});
			
			//Check listeners for emptiness
			$(this).find('.check-listener').each(function(){
				if(!this.value)
				{
					listenerComplete = 0;
					$('#listener_error').html('Не всем слушателям указано ФИО!' );
					$('#listener_error').show();
					return false;
				}
				else
				{
					$('#listener_error').hide();
				}
			});
			console.log(complete);
			console.log(listenerComplete);
			console.log(self.DOM.globalComplete);
			if(!complete || !listenerComplete || !self.DOM.globalComplete)
			{
				alert('Заполнены не все обьязательные поля!');
				return false;
			}
		});
	}
}
var MeListener = {
	init:function()
	{
		//Set me as listener
		$('.add-listener').on('click',function(){
			$('.listener-container').children().first().val($('.contact_name').val());
		});
	}
}
var ListenerCount = {
	init:function()
	{
		var option, price;
		$('.form-group').on('change','.count_place',function(){
			var count = this.value,
				oneInput = $($('.listener-container').children().first().get(0)).clone(), //Listener input
				$container = $('.listener-container').clone();  //Listener container
			$container.empty();
			
			//Generate new container
			for (var i = 0; i < count; i++) {
				if (i != 0) {
					oneInput.val('');
				}
				$container.append(oneInput.clone());
			}
			
			//Change DOM container by JS container
			$('.listener-container').replaceWith($container);
			
			//change fullPrice
			option = document.getElementById('id_group_'+$('.id_group').val());
			price = parseFloat(option.getAttribute('data-group-price') * $(this).val());
			$('.full_price').html(price.toFixed(2));
		});
	}
}

var ChangePrice = {
	init:function()
	{
		var groupOption, price, fullPrice, seatsFree, countOptions = '', oneInput,
			countSelect = $('.count_place').clone();
		
		//Change groupPrice and fullPrice when list of groups is changing
		$('.id_group').on('change',function(){
			
			//Empty listeners container, except first
			oneInput = $($('.listener-container').children().first().get(0)).clone();
			$('.listener-container').empty().append(oneInput);
			
			//Get price and seats data 
			groupOption = document.getElementById('id_group_'+$(this).val());
			price = parseFloat(groupOption.getAttribute('data-group-price'));
			seatsFree = parseFloat(groupOption.getAttribute('data-group-seatsfree'));
			
			//Generate new seats select
			countOptions = '';
			for(i = 1; i < seatsFree+1; i++){
				countOptions += '<option value="'+i+'">'+i+'</option>';
			}
			
			//Put new select to the DOM
			$(countSelect).empty().wrapInner(countOptions);
			$('select.count_place').replaceWith(countSelect);
			
			//Put prices into html
			fullPrice = price * $('select.count_place').val();
			$('.group_price').html(price.toFixed(2));
			$('.full_price').html(fullPrice.toFixed(2));
		});
	}
}
$(document).ready(function(){
	
	FormValidation.init();
	MeListener.init();
	ListenerCount.init();
	ChangePrice.init();
	
	//Put default number, when document is ready!
	var option = document.getElementById('id_group_'+$('.id_group').val());
	var price = parseFloat(option.getAttribute('data-group-price'));
	$('.group_price').html(price.toFixed(2));
	$('.full_price').html(price.toFixed(2));
});