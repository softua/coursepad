Исходный код сайта [coursepad.ru](http://coursepad.ru)

Содержание .env.php файла:

    <?php
    
    return [
        'DEBUG' => true, // отображение дебаг информации
    
        'APP_URL' => 'http://coursepad.localhost', // URL приложения для отработки команд artisan
    
        'ENC_KEY' => 'FHRjDnkDpERnzQkFTruyhFnzxdsmEd', // 30 значный ключ для шифрования сессий
    
        'DB_HOST' => 'localhost', // хост БД
        'DB_NAME' => 'coursepad', // название БД
        'DB_USER' => 'homestead', // пользователь БД
        'DB_PASS' => 'secret',    // пароль пользователя
    
        // Настройки почты
        'MAIL_DRIVER' => 'mandrill',
        'MANDRILL_KEY' => 'SoMeSeCrEtKey', // ключ к Сервису
        
        // данные для робокассы
        'ROBOKASSA_URL' => 'http://test.robokassa.ru/Index.aspx',
        'ROBOKASSA_LOGIN' => 'coursepad',
        'ROBOKASSA_PASS1' => 'qwerty12345',
        'ROBOKASSA_PASS2' => 'qwerty1234567890',
    ];